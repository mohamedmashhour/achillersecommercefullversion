<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

use Illuminate\Support\Facades\Storage;
use App\CmsPage;
use App\Category;
use Illuminate\Support\Facades\Mail;
use Validator;
use Image;
use App\Enquiry;
use App\Template;

class CmsController extends Controller
{
    public function addCmsPage(Request $request){
    	if($request->isMethod('post')){
    		$data = $request->all();
    		echo "<pre>"; print_r($data); die;
            if(empty($data['meta_title'])){
                $data['meta_title'] = "";    
            }
            if(empty($data['meta_description'])){
                $data['meta_description'] = "";    
            }
            if(empty($data['meta_keywords'])){
                $data['meta_keywords'] = "";    
            }
    		$cmspage = new CmsPage;
    		$cmspage->title = $data['title'];
    		$cmspage->url = $data['url'];
            $cmspage->description = $data['description'];
            $cmspage->meta_title = $data['meta_title'];
            $cmspage->meta_description = $data['meta_description'];
    		$cmspage->meta_keywords = $data['meta_keywords'];
    		if(empty($data['status'])){
    			$status = 0;
    		}else{
    			$status = 1;
    		}
    		$cmspage->status = $status;
    		$cmspage->save();
    		return redirect()->back()->with('flash_message_success','CMS Page has been added successfully');
    	}
    	return view('admin.pages.add_cms_page');
    }

    public function editCmsPage(Request $request,$id){
        if($request->isMethod('post')){
            $data = $request->all();
            if(empty($data['status'])){
                $status = 0;
            }else{
                $status = 1;
            }
            if(empty($data['meta_title'])){
                $data['meta_title'] = "";    
            }
            if(empty($data['meta_description'])){
                $data['meta_description'] = "";    
            }
            if(empty($data['meta_keywords'])){
                $data['meta_keywords'] = "";    
            }
            CmsPage::where('id',$id)->update(['title'=>$data['title'],'url'=>$data['url'],'description'=>$data['description'],'meta_title'=>$data['meta_title'],'meta_description'=>$data['meta_description'],'meta_keywords'=>$data['meta_keywords'],'status'=>$status]);
            return redirect()->back()->with('flash_message_success','CMS Page has been updated successfully!');
        }
        $cmsPage = CmsPage::where('id',$id)->first();
        return view('admin.pages.edit_cms_page')->with(compact('cmsPage'));
    }

    public function viewCmsPages(){
        $cmsPages = CmsPage::get();
        return view('admin.pages.view_cms_pages')->with(compact('cmsPages'));
    }

    public function deleteCmsPage($id){
        CmsPage::where('id',$id)->delete();
        return redirect('/admin/view-cms-pages')->with('flash_message_success','CMS Page has been deleted successfully!');
    }

    public function cmsPage($url){

        // Redirect to 404 if CMS Page is disabled or does not exists
        $cmsPageCount = CmsPage::where(['url'=>$url,'status'=>1])->count();
        if($cmsPageCount>0){
            // Get CMS Page Details
            $cmsPageDetails = CmsPage::where('url',$url)->first();
            $meta_title = $cmsPageDetails->meta_title;
            $meta_description = $cmsPageDetails->meta_description;
            $meta_keywords = $cmsPageDetails->meta_keywords;
        }else{
            abort(404);    
        }

        // Get All Categories and Sub Categories
        $categories_menu = "";
        $categories = Category::with('categories')->where(['parent_id' => 0])->get();
        $categories = json_decode(json_encode($categories));
        /*echo "<pre>"; print_r($categories); die;*/
        foreach($categories as $cat){
            $categories_menu .= "
            <div class='panel-heading'>
                <h4 class='panel-title'>
                    <a data-toggle='collapse' data-parent='#accordian' href='#".$cat->id."'>
                        <span class='badge pull-right'><i class='fa fa-plus'></i></span>
                        ".$cat->name."
                    </a>
                </h4>
            </div>
            <div id='".$cat->id."' class='panel-collapse collapse'>
                <div class='panel-body'>
                    <ul>";
                    $sub_categories = Category::where(['parent_id' => $cat->id])->get();
                    foreach($sub_categories as $sub_cat){
                        $categories_menu .= "<li><a href='#'>".$sub_cat->name." </a></li>";
                    }
                        $categories_menu .= "</ul>
                </div>
            </div>
            ";      
        }

        return view('pages.cms_page')->with(compact('cmsPageDetails','categories_menu','categories','meta_title','meta_keywords','meta_description'));
    }

    public function contact(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/

            $validator = Validator::make($request->all(), [
                'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
                'email' => 'required|email',
                'subject' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            // Send Contact Email
            $email = "admin1000@yopmail.com";
            $messageData = [
                'name'=>$data['name'],
                'email'=>$data['email'],
                'subject'=>$data['subject'],
                'comment'=>$data['message']
            ];
            Mail::send('emails.enquiry',$messageData,function($message)use($email){
                $message->to($email)->subject('Enquiry from E-com Website');
            });

            return redirect()->back()->with('flash_message_success','Thanks for your enquiry. We will get back to you soon.');
        }

        // Get All Categories and Sub Categories
        $categories_menu = "";
        $categories = Category::with('categories')->where(['parent_id' => 0])->get();
        $categories = json_decode(json_encode($categories));
        /*echo "<pre>"; print_r($categories); die;*/
        foreach($categories as $cat){
            $categories_menu .= "
            <div class='panel-heading'>
                <h4 class='panel-title'>
                    <a data-toggle='collapse' data-parent='#accordian' href='#".$cat->id."'>
                        <span class='badge pull-right'><i class='fa fa-plus'></i></span>
                        ".$cat->name."
                    </a>
                </h4>
            </div>
            <div id='".$cat->id."' class='panel-collapse collapse'>
                <div class='panel-body'>
                    <ul>";
                    $sub_categories = Category::where(['parent_id' => $cat->id])->get();
                    foreach($sub_categories as $sub_cat){
                        $categories_menu .= "<li><a href='#'>".$sub_cat->name." </a></li>";
                    }
                        $categories_menu .= "</ul>
                </div>
            </div>
            ";      
        }

        $meta_title = "Contact Us - E-shop Sample Website";
        $meta_description = "Contact us for any queries related to our products.";
        $meta_keywords = "contact us, queries";
        return view('pages.contact')->with(compact('categories_menu','categories','meta_title','meta_description','meta_keywords'));
    }

    public function addPost(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/
            $enquiry = new Enquiry;
            $enquiry->name = $data['name'];
            $enquiry->email = $data['email'];
            $enquiry->subject = $data['subject'];
            $enquiry->message = $data['message'];
            $enquiry->save();
            echo "Thanks for contacting us. We will get back to you soon."; die;
        }

        // Get All Categories and Sub Categories
        $categories_menu = "";
        $categories = Category::with('categories')->where(['parent_id' => 0])->get();
        $categories = json_decode(json_encode($categories));
        /*echo "<pre>"; print_r($categories); die;*/
        foreach($categories as $cat){
            $categories_menu .= "
            <div class='panel-heading'>
                <h4 class='panel-title'>
                    <a data-toggle='collapse' data-parent='#accordian' href='#".$cat->id."'>
                        <span class='badge pull-right'><i class='fa fa-plus'></i></span>
                        ".$cat->name."
                    </a>
                </h4>
            </div>
            <div id='".$cat->id."' class='panel-collapse collapse'>
                <div class='panel-body'>
                    <ul>";
                    $sub_categories = Category::where(['parent_id' => $cat->id])->get();
                    foreach($sub_categories as $sub_cat){
                        $categories_menu .= "<li><a href='#'>".$sub_cat->name." </a></li>";
                    }
                        $categories_menu .= "</ul>
                </div>
            </div>
            ";      
        }

        return view('pages.post')->with(compact('categories_menu','categories'));
    }

    public function getEnquiries(){
        $enquiries = Enquiry::orderBy('id','Desc')->get();
        $enquiries = json_encode($enquiries);
        return $enquiries;
    }

    public function viewEnquiries(){
        return view('admin.enquiries.view_enquiries');
    }

    public function updateMenu(Request $request){
        
        if($request->isMethod('post')){

            $data = $request->all();
            if(empty($data['value'])){
                $data['value'] = "";    
            }
            Storage::put('public/header.json',$data['value']);
            // $jsonString = Storage::get('public\header.json');
            // $data = json_decode($jsonString, true);
            // dd($data) ;
            // Template::where(['key'=>'header_menu'])->update(['value'=>$data['value']]);
            return redirect()->back()->with('flash_message_success','Header Menu has been updated successfully!');
            
        }
        if(File::exists('public\header.json')){
            $jsonString = Storage::get('public\header.json');

            return view('admin.pages.headerCms')->with(compact('jsonString'));
            
        }else{
            $jsonString = '';
            return view('admin.pages.headerCms')->with(compact('jsonString'));
        }
        
    }

    public function updateLogo(Request $request){ 
    
            if($request->isMethod('post')){

                $data = $request->all();
    
                if($request->hasFile('logo')){
                    $image_tmp = $request->file('logo');
                    if ($image_tmp->isValid()) {
                        // Upload Images after Resize
                        $extension = $image_tmp->getClientOriginalExtension();
                        $fileName = rand(111,99999).'.'.$extension;
                        $medium_image_path = 'images/backend_images'.'/'.$fileName;
                        Image::make($image_tmp)->resize(150, 150)->save($medium_image_path);


                    }
                
                }else if(!empty($data['current_image'])){
                    $fileName = $data['current_image'];
                }else{
                    $fileName = '';
                }

            Template::where(['key'=>'header_logo'])->update(['value'=>$fileName]);
            return redirect()->back()->with('flash_message_success','Header logo has been updated successfully!');
        }
        $logo = Template::where(['key'=>'header_logo'])->first();
        $logo = $logo['value'];
        return view('admin.pages.logoCms')->with(compact('logo'));

        
    }

    public function footercontact(Request $request){
       
        if($request->isMethod('post')){
            
            // dd($request);
            $data = $request->all();

            
                Template::where(['key'=>'footer_cont_phone'])->update(['value'=>$data['phone']]);
                Template::where(['key'=>'footer_cont_address'])->update(['value'=>$data['address']]);
                Template::where(['key'=>'footer_cont_email'])->update(['value'=>$data['email']]);
                Template::where(['key'=>'footer_linkes'])->update(['value'=>$data['links']]);
                Template::where(['key'=>'footer_social'])->update(['value'=>$data['social']]);
                Template::where(['key'=>'footer_widget1'])->update(['value'=>$data['widget1']]);
                Template::where(['key'=>'footer_widget2'])->update(['value'=>$data['widget2']]);
    
                return redirect()->back()->with('flash_message_success','Header Menu has been updated successfully!');
            
            
        }
        
        $footerphone =  Template::where(['key'=>'footer_cont_phone'])->get('value');
        $footeradd =  Template::where(['key'=>'footer_cont_address'])->get('value');
        $footermail =  Template::where(['key'=>'footer_cont_email'])->get('value');
        $footerlinks =  Template::where(['key'=>'footer_linkes'])->get('value');
        $footersocial =  Template::where(['key'=>'footer_social'])->get('value');
        $footerwid1 =  Template::where(['key'=>'footer_widget1'])->get('value');
        $footerwid2 =  Template::where(['key'=>'footer_widget2'])->get('value');
        return view('admin.pages.footer')->with(compact('footerphone','footeradd','footermail','footerlinks','footersocial','footerwid1','footerwid2'));
    }

    public function productCms(Request $request){
       
        if($request->isMethod('post')){
            
            // dd($request);
            $data = $request->all();

            if($request->hasFile('pro1')){
                $image_tmp = $request->file('pro1');
                if ($image_tmp->isValid()) {
                    // Upload Images after Resize
                    $extension = $image_tmp->getClientOriginalExtension();
                    $pro1 = rand(111,99999).'.'.$extension;
                    $medium_image_path = 'images/backend_images'.'/'.$pro1;
                    Image::make($image_tmp)->resize(150, 150)->save($medium_image_path);


                }
            
            }else if(!empty($data['current_image'])){
                $pro1 = $data['current_image'];
            }else{
                $pro1 = '';
            }

            if($request->hasFile('pro2')){
                $image_tmp = $request->file('pro2');
                if ($image_tmp->isValid()) {
                    // Upload Images after Resize
                    $extension = $image_tmp->getClientOriginalExtension();
                    $pro2 = rand(111,99999).'.'.$extension;
                    $medium_image_path = 'images/backend_images'.'/'.$pro2;
                    Image::make($image_tmp)->resize(150, 150)->save($medium_image_path);


                }
            
            }else if(!empty($data['current_image'])){
                $pro2 = $data['current_image'];
            }else{
                $pro2 = '';
            }

            if($request->hasFile('shop')){
                $image_tmp = $request->file('shop');
                if ($image_tmp->isValid()) {
                    // Upload Images after Resize
                    $extension = $image_tmp->getClientOriginalExtension();
                    $shop = rand(111,99999).'.'.$extension;
                    $medium_image_path = 'images/backend_images'.'/'.$shop;
                    Image::make($image_tmp)->resize(150, 150)->save($medium_image_path);


                }
            
            }else if(!empty($data['current_image'])){
                $shop = $data['current_image'];
            }else{
                $shop = '';
            }

            if($request->hasFile('size')){
                $image_tmp = $request->file('size');
                if ($image_tmp->isValid()) {
                    // Upload Images after Resize
                    $extension = $image_tmp->getClientOriginalExtension();
                    $size = rand(111,99999).'.'.$extension;
                    $medium_image_path = 'images/backend_images'.'/'.$size;
                    Image::make($image_tmp)->resize(150, 150)->save($medium_image_path);


                }
            
            }else if(!empty($data['current_image'])){
                $size = $data['current_image'];
            }else{
                $size = '';
            }



              
                Template::where(['key'=>'shop_banner'])->update(['value'=>$shop]);
                Template::where(['key'=>'product_size_guide'])->update(['value'=>$size]);
                Template::where(['key'=>'product_promotion1'])->update(['value'=>$pro2]);
                Template::where(['key'=>'product_promotion2'])->update(['value'=>$pro1]);

    
                return redirect()->back()->with('flash_message_success','Image promotion  has been updated successfully!');
            
            
        }

        $shop =  Template::where(['key'=>'shop_banner'])->get('value');
        $size =  Template::where(['key'=>'product_size_guide'])->get('value');
        $pro1 =  Template::where(['key'=>'product_promotion1'])->get('value');
        $pro2 =  Template::where(['key'=>'product_promotion2'])->get('value');


        return view('admin.pages.product_cms')->with(compact('shop', 'size','pro1','pro2'));
    }
    
}