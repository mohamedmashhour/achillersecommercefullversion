<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AffiliateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:affilate');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}

// namespace App\Http\Controllers;

// use Illuminate\Http\Request;

// use App\AffiliateUser;

// class AffiliateController extends Controller
// {
//     public function newAffiliate(Request $request){

//         if($request->isMethod('post')){

//             $data = $request->all();
//             // echo "<pre>"; print_r($data); die;
//             $affiliate = new AffiliateUser;

//             $affiliate->name = $data['name'];
//             $affiliate->email = $data['email'];
//             $affiliate->password = bcrypt($data['password']); 
//             $affiliate->save();

//             }

//         return view('affiliate.newaffiliate');
//     }

//     public function dashborad(){
//         $user_id = Auth::user()->id;
//         $userDetails = User::find($user_id);
//         return view('affiliate.affiliate_dashboard');
//     }
// }
