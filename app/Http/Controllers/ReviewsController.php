<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Reviews;
use App\Product;

class ReviewsController extends Controller
{
    public function addReview(Request $request){

        if($request->isMethod('post')){
            $data = $request->all();

            $reviews = new Reviews;
			$reviews->product_id = $data['product_id'];
			$reviews->rate = $data['rate'];
			$reviews->user_email = $data['user_email'];
            $reviews->comment = $data['comment'];
            $reviews->save();
			return redirect()->back()->with('flash_message_success', 'rate has been added successfully');

        }

    }
}
