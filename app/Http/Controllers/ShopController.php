<?php

namespace App\Http\Controllers;
use App\ProductsImage;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Storage;
use App\Banner;
use App\Category;
use App\Product;
use App\ProductsAttribute;
use App\Reviews;


class ShopController extends Controller
{
    public function index(){
		$featProducts = Product::inRandomOrder()->where('status',1)->where('feature_item',1)->paginate(12);

            // Get all Products
			$productsAll = Product::where('status',1)->get();


			foreach($productsAll as $key => $val){
				$category_name = Category::where(['id' => $val->category_id])->first();
				$productsAll[$key]->category_name = $category_name->name;
			}
			
			foreach($productsAll as $key => $val){
				
				$product_images = ProductsImage::where('product_id',$val->id)->get('image');
				
				
				$productsAll[$key]->product_images = $product_images;

			}
			
			foreach($productsAll as $key => $val){
				
				$product_attr = ProductsAttribute::select('size','stock')->where('product_id',$val->id)->get();

				
				$productsAll[$key]->product_attr = $product_attr;
			}
			// foreach($productsAll as $key => $val){
				
			// 	$productsAll[$key]->wishlist_link = "";
			// }

			foreach($productsAll as $key => $val){

				$product_rate = Reviews::where('product_id',$val->id)->get();

				// dd($product_rate ->sum());
				// $rate = $product_rate->rate;
				$total_product_rate = $product_rate->sum('rate');
				$num_product_rate = $product_rate->count('rate');

				if($num_product_rate > 0){
					$product_rate = $total_product_rate / $num_product_rate;
					$product_rate = round($product_rate);
				}else{
					$product_rate = 0;
				}
				
				// foreach($product_rate as $rate){
				// 	$product_rate = $rate->rate;
				// 	$product_rate = $product_rate->sum();

				// 	// dd($product_rate);
				// }
				$productsAll[$key]->product_rate = $product_rate;
			}
			
			// dd($productsAll);
			// $productsAll = Response::json($productsAll);
			
			Storage::put('public/product.json',$productsAll);


    	/*dump($productsAll);*/
    	/*echo "<pre>"; print_r($productsAll);die;*/


    	// Get All Categories and Sub Categories
    	$categories_menu = "";
    	$categories = Category::with('categories')->where(['parent_id' => 0])->get();
    	$categories = json_decode(json_encode($categories));
    	/*echo "<pre>"; print_r($categories); die;*/
		foreach($categories as $cat){
			$categories_menu .= "
			<div class='panel-heading'>
				<h4 class='panel-title'>
					<a data-toggle='collapse' data-parent='#accordian' href='#".$cat->id."'>
						<span class='badge pull-right'><i class='fa fa-plus'></i></span>
						".$cat->name."
					</a>
				</h4>
			</div>
			<div id='".$cat->id."' class='panel-collapse collapse'>
				<div class='panel-body'>
					<ul>";
					$sub_categories = Category::where(['parent_id' => $cat->id])->get();
					foreach($sub_categories as $sub_cat){
						$categories_menu .= "<li><a href='#'>".$sub_cat->name." </a></li>";
					}
						$categories_menu .= "</ul>
				</div>
			</div>
			";		
		}

		$banners = Banner::where('status','1')->get();
		// Meta tags
		$meta_title = "E-shop Sample Website";
		$meta_description = "Online Shopping Site for Men, Women and Kids Clothing";
		$meta_keywords = "eshop website, online shopping, men clothing";
    	return view('shop')->with(compact('categories_menu','categories','banners','meta_title','meta_description','meta_keywords' ,'featProducts'));
        
        // my function
    //     // Get all Products
    //     $productsAll = Product::inRandomOrder()->where('status',1)->where('feature_item',1)->paginate();
    //     // dd($productsAll);
    //     return view('shop')->with(compact('productsAll','categories_menu','categories','banners','meta_title','meta_description','meta_keywords'));
    //
 }
//  function load_data(Request $request)
//     {

		
// 		if($request->ajax())
// 		{

// 			$productsAll = Product::where('status',1)->get()->toJson();
// 			Storage::put('public/product.json',$productsAll);
// 			echo asset('storage/product.json');
// 			// Storage::disk('public')->put('product.json', $productsAll);
// 			print_r('sucess');

// 		}

//     }
}