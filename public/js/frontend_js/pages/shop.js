
$(document).ready(function(){
    $(".first-body .body__footer .fonts a").on('click',function(){

        $(this).parents('.first-body').remove();
    });
    
    $(".plus").click(function(){
        var inputVal = $(this).parent().siblings().val();
        inputVal ++; 
        $(this).parent().siblings().attr('value', inputVal);
    });

    $(".minus").click(function() {
        var inputVal = $(this).parent().siblings().val();
        if( inputVal > 1) {
            inputVal --;
            $(this).parent().siblings().attr('value', inputVal);
        }
    });
});