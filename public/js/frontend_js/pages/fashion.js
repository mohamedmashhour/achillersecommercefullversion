$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('.owl-products').owlCarousel({
        items: 1,
        loop: true,
        rtl:true,
    });



    var mainProductContainer = $('.append-ajax-content');

    $(document).on('click', '.col-items', function() {
        mainProductContainer.removeClass('row-products');
        mainProductContainer.addClass('col-products');

        $(this).addClass('opacity-half');
        $('.row-items').removeClass('opacity-half');
    });

    $(document).on('click', '.row-items', function() {
        mainProductContainer.removeClass('col-products');
        mainProductContainer.addClass('row-products');

        $(this).addClass('opacity-half');
        $('.col-items').removeClass('opacity-half');

    });
















    $(document).on('click', '.fashion__right .products-info .head-content .amount', function() {
        var productImg = $(this).siblings('img:first').attr('src');
        var productTitle = $(this).parents('.head-content').siblings('.coulom-content').find('.product-content__head').text();
        var productPrice = $(this).parents('.head-content').siblings('.coulom-content').find('.product-content__price').text();
        var productParent = $(this).parents('.main-coulom');
        var productId = $(this).parents('.main-coulom').attr('id');

        if (productParent.hasClass('add-compare')) {

        } else {

            productParent.addClass('add-compare');
            var features = $('.compare-modal__product__details .features');

            var featuresContent = `<td class="${productId}">
                <img src="${productImg}" alt="img">
                <h1 class="features__title">${productTitle}</h1>
                <button class="delete-product-compare" type="button">
                <span><i class="far fa-trash-alt"></i></span>
                </button>
            </td>`;

            features.append(featuresContent);


            var avilability = $('.compare-modal__product__details .avilability');

            var avilabilityContent = `<td class="${productId}"></td>`;

            avilability.append(avilabilityContent);


            var color = $('.compare-modal__product__details .colors');

            var productColor = $(this).closest('.main-coulom').find('.product-content__info__hidden-colors').html();


            var colorContent = `<td class="${productId}">
                ${productColor}
            </td>`;

            color.append(colorContent);




            var price = $('.compare-modal__product__details .price');

            var priceContent = `<td class="${productId}">
                <span class="pri">${productPrice}</span>
            </td>`;

            price.append(priceContent);

            var options = $('.compare-modal__product__details .options');


            var productSize = $(this).closest('.main-coulom').find('.product-content__info__hidden-sizes').html();

            var optionsContent = `<td class="${productId}">
                ${productSize}
            </td>`;

            options.append(optionsContent);


            var action = $('.compare-modal__product__details .action');

            var actionContent = `<td class="${productId}">
                <div class="d-flex"><span>items selected : </span><span data-class="${productId}" class="quick-selected-items"></span></div>
                <button class="quickview-action" data-class="${productId}" type="button">add to cart</button>
            </td>`;

            action.append(actionContent);


        }

        $('.compare-modal__title__span').text(productTitle);
        if ($('.compare-modal__product__details .features td').length > 1) {
            $('.compare-modal__warning').addClass('d-none');
        }

        $('.compare-modal__count').text($('.compare-modal__product__details .features td').length - 1)



        var buttonHasDataAttr = $(this).attr('data-class');

        var allSpanQty = $(`.quick-selected-items[data-class="${buttonHasDataAttr}"]`);

        var productSelectedQty = $(this).closest('.main-coulom').find('.product-content__add-cart__number input').attr('value');


        allSpanQty.text(productSelectedQty);


        

        
    });

    // function quickview action

    $(document).on('click', '.quickview-action', function() {
        var btnActionDataClass = $(this).attr('data-class');
        var relatedProduct = $(`.main-coulom[data-id="${btnActionDataClass}"]`);
        relatedProduct.find('.product-content__add-cart button').click();
    });

    $(document).on('click', '.delete-product-compare', function() {
        var productClass = $(this).closest('td').attr('class');
        var productsClasses = $(`*[class="${productClass}"]`);
        var productRelatedClass = $(`#${productClass}`);
        productsClasses.remove();
        productRelatedClass.removeClass('add-compare');
        if ($('.compare-modal__product__details .features td').length <= 1) {
            $('.compare-modal__warning').removeClass('d-none');
        }
        $('.compare-modal__count').text($('.compare-modal__product__details .features td').length - 1)

    });


    

    // quick view modal
    var zoomMainDiv = $('.exzoom');
    $(document).on('click', '.product-content .view,.main-layout', function() {
        zoomMainDiv.empty();
        var productImg = $(this).closest('.main-coulom').find('.product-content__img img:first').attr('src');
        var exzoomContent = `<div class="exzoom_img_box"><ul class='exzoom_img_ul'></ul></div>
        <div class="exzoom_nav"></div>`;
        zoomMainDiv.append(exzoomContent);
        var ZoomList = $('.exzoom_img_ul');
        var AllProductImgs = $(this).closest('.main-coulom').find('.product-content__info__hidden-imgs img');

        var imgUrl = `<img src="${productImg}" alt="product img">`
        if (AllProductImgs.length < 1) {
            $('.background-right-qickview').html(imgUrl);
            $('.background-right-qickview').removeClass('d-none');
        } else {
            $('.background-right-qickview').addClass('d-none');
        }
        AllProductImgs.each(function() {
            var li = `<li><img class="preview" src="${$(this).attr('src')}"></li>`;
            ZoomList.append(li);
        });
                    
            
        if (AllProductImgs.length > 0) {
            $("#exzoom").exzoom({
                "autoPlay":false,

            });
        }


        var productHead = $(this).closest('.main-coulom').find('.product-content__head').text();
        $('.quick-view__product__right .head').text(productHead);

        var productPrice = $(this).closest('.main-coulom').find('.product-content__price').text();
        $('.quick-view__product__right .price span').text("Price " + productPrice);

        var productContent = $(this).closest('.main-coulom').find('.product-content__content').text();
        $('.quick-view__product__right .par').text(productContent);


        var productSize = $(this).closest('.main-coulom').find('.product-content__info__hidden-sizes').html();
        
        $('.quick-view__product__right .general-sidebar__left__size').html(productSize);

        productColor = $(this).closest('.main-coulom').find('.product-content__info__hidden-colors span').attr('data-color');
        var SpanColor = `<span style="background-color:#${productColor}"></span>`;

        $('.quick-view__product__right .general-sidebar__left__colors').html(SpanColor);

        var stockOfProduct = $(this).closest('.main-coulom').find('.product-content__info__hidden-sizes span');

        var productStockVal;
        stockOfProduct.each(function() {
            productStockVal = parseInt($(this).attr('data-stock'));
            productStockVal += productStockVal;
        });

        if ($(this).closest('.main-coulom').find('.product-content__info__hidden-sizes > *').length > 0) {

            if (productStockVal == 0) {
                var span = `<span>out of stock</span>`;
                $('.quick-view__product__right .details').html(span);
            }
        }
        

        // function return number of star

        var productStar = parseInt($(this).closest('.main-coulom').find('.product-content__star').attr('data-star'));

        var quickviewStar = $('.quick-view__product__right .star span');

        for (t=0;t<productStar;t++){
            quickviewStar[t].classList.add('review-star');
        }

        if (productStar == 0) {
            $('.quick-view__product__right__no-review').removeClass('d-none');
        } else {
            $('.quick-view__product__right__no-review').addClass('d-none');
        }


        var productAttr = $(this).closest('.main-coulom').attr('data-id');


        var viewModalBtn = $('.add-product__add-cart');

        viewModalBtn.attr('data-id', productAttr);


        var productSelectedQty = $(this).closest('.main-coulom').find('.product-content__add-cart__number input').attr('value');

        $('.quick-view__product__right__selected-items__qty').text(productSelectedQty);
       
    });


    // compare between modal product and shop product
    var viewModalBtn = $('.add-product__add-cart');
    viewModalBtn.click(function() {
        var relatedProduct = $(`.main-coulom[data-id="${viewModalBtn.attr('data-id')}"]`);
        var productBtn = relatedProduct.find('.product-content__add-cart button');
        productBtn.click();
    });

    
    




    // function add product
    var productCaretCounter = 0;
    if (sessionStorage.length != 0) {
        var newCounter = parseInt(sessionStorage.getItem('data-counter'));

        productCaretCounter = newCounter;
    }
    var caretSpan = $('.nav-bar__right__menu__bag-product__btn span');
    var caretItemsSpan = $('.nav-bar__right__menu__bag-product__content__head__count');

    caretSpan.text(productCaretCounter);



    $(document).on('click', '.product-content__add-cart button',function(e) {
        e.stopPropagation();




        var inputVal = parseInt($(this).closest('.product-content__add-cart').find('input').val());
        var mainProduct = $(this).closest('.main-coulom');
        var productDataId = $(this).closest('.main-coulom').attr('id');
        var productBagContainer = $('.nav-bar__right__menu__bag-product__content__product');
        var productCode = $(this).closest('.main-coulom').attr('data-code');
        var productColor = $(this).closest('.main-coulom').attr('data-color');
        var productId = $(this).closest('.main-coulom').attr('data-send-id')
        var productTitle = $(this).closest('.coulom-content').find('.product-content__head').text();
        var productPrice = $(this).closest('.coulom-content').find('.product-content__price').text();
        var productImg = $(this).closest('.main-coulom').find('img:first').attr('src');

        var productSelectVal = $(this).closest('.main-coulom').find('.product-content__add-cart__select select option:selected').attr('value');

        if (productSelectVal == 0) {
            var productSelected = $(this).closest('.main-coulom').find('.product-content__add-cart__select');
            $('.product-content__add-cart__select').removeClass('border-red').not(productSelected).removeClass('border-red');
            productSelected.addClass('border-red');

            
        } else {
            $(this).closest('.main-coulom').find('.product-content__add-cart__select').removeClass('border-red');
        }


        if (productSelectVal != 0) {
            $('.nav-bar__right__menu__bag-product').addClass('show-product-content');
            $('.nav-bar__right__menu__bag-product__btn span').removeClass('d-none');
            $('.nav-bar__right__menu__bag-product__content__product__empty').addClass('d-none');
            var productInfo = `<div data-val="${productDataId}" class="container-fluid nav-bar__right__menu__bag-product__content__product--container">
    


                    <input type="hidden" name="product_id[]" value="${productId}">

                    <input type="hidden" name="product_name[]" value="${productTitle}">
                    
                    <input type="hidden" name="product_code[]" value="${productCode}">
                    
                    <input type="hidden" name="product_color[]" value="${productColor}">
                    
                    <input type="hidden" name="price[]" value="${productPrice}">
                    
                    <input class="input-product-size" name="size[]" type="hidden" value="${productSelectVal}">
                    
                    <input class="input-product-qty" type="hidden" name="quantity[]" value="${inputVal}">
            

                  

                    <div class="row">
                        <div class="col-6 nav-bar__right__menu__bag-product__content__product__left">
                            <div>
                                <a href="#">${productTitle}</a>
                            </div>
                            <div>
                                <span class="nav-bar__right__menu__bag-product__content__product__left__count">${inputVal}</span>
                                <span>x</span>
                                <span class="nav-bar__right__menu__bag-product__content__product__left__price">${productPrice}</span>
                            </div>
                        </div>
                        <div class="col-6 d-flex justify-content-end nav-bar__right__menu__bag-product__content__product__right">
                            <a href="#">
                                <img src="${productImg}" alt="img">
                            </a>
                            <button type="button" class="nav-bar__right__menu__bag-product__content__product__right__delete"></button>
                        </div>
                    </div>
                </div>`;

            if (mainProduct.hasClass('product-added')) {
                
                var relatedBagInputProduct = $(`.nav-bar__right__menu__bag-product__content__product--container input[value="${productSelectVal}"]`);
    
                if (relatedBagInputProduct.length == 0) {
                    
                    productBagContainer.append(productInfo);
                } else {
                    var spanQty = relatedBagInputProduct.closest('.nav-bar__right__menu__bag-product__content__product--container').find('.input-product-qty');

                    var spanCount = relatedBagInputProduct.closest('.nav-bar__right__menu__bag-product__content__product--container').find('.nav-bar__right__menu__bag-product__content__product__left__count');
                    var valueSpanCount = parseInt(spanCount.text());

                    valueSpanCount += inputVal;

                    spanCount.text(valueSpanCount);

                    spanQty.attr('value', valueSpanCount);



                }
    
    
            } else {
                productBagContainer.append(productInfo);
                mainProduct.addClass('product-added');
            }




            // increase value when click add cart
            productCaretCounter += inputVal;
            caretSpan.text(productCaretCounter);
            caretItemsSpan.text(productCaretCounter);

            sessionStorage.setItem('data-counter', productCaretCounter);

            
            var totalPriceSection = parseInt($('.nav-bar__right__menu__bag-product__content__footer__info .total-price').text());


            var PriceExpected = productPrice * inputVal;

            var totalPrice = totalPriceSection + PriceExpected

            

            $('.nav-bar__right__menu__bag-product__content__footer__info .total-price').text(totalPrice);

            sessionStorage.setItem('totalPrice', totalPrice)
        } 
       
        var allSavedProduct = $('.nav-bar__right__menu__bag-product__content__product');
        sessionStorage.setItem('allSavedProduct', allSavedProduct.html());

    });


    $('.nav-bar__right__menu__bag-product__content__footer__info .total-price').text(sessionStorage.getItem('totalPrice'))

    $(document).on('click', '.nav-bar__right__menu__bag-product__content__product__right__delete', function() {



        var productContainer = $(this).closest('.nav-bar__right__menu__bag-product__content__product--container');
        var relatedProductId = productContainer.attr('data-val');
        var relatedProduct = $(`#${relatedProductId}`);
        var productContainerCounterSpan = productContainer.find('.nav-bar__right__menu__bag-product__content__product__left__count');
        var productContainerCounterVal = parseInt(productContainerCounterSpan.text());
    

        console.log(productContainerCounterVal);
        productCaretCounter -= productContainerCounterVal;
        caretSpan.text(productCaretCounter);
        caretItemsSpan.text(productCaretCounter);

        var totalPriceSection = parseInt($('.nav-bar__right__menu__bag-product__content__footer__info .total-price').text());
        var productQty = parseInt($(this).closest('.nav-bar__right__menu__bag-product__content__product--container').find('.nav-bar__right__menu__bag-product__content__product__left__count').text());
        var productPrice = parseInt($(this).closest('.nav-bar__right__menu__bag-product__content__product--container').find('.nav-bar__right__menu__bag-product__content__product__left__price').text());

        console.log(productQty);
        console.log(productPrice);
        
        var PriceExpected = productPrice * productQty;

        var totalPrice = totalPriceSection - PriceExpected

        

        $('.nav-bar__right__menu__bag-product__content__footer__info .total-price').text(totalPrice);

        sessionStorage.setItem('totalPrice', totalPrice);
    
        // Remove product container
        productContainer.remove();

        var containerWithAttr = $(this).closest('.nav-bar__right__menu__bag-product__content__product--container[data-val]');

        if (containerWithAttr.length == 0) {
            relatedProduct.removeClass('product-added');
        }
    
        
    
    

    
        var allSavedProduct = $('.nav-bar__right__menu__bag-product__content__product');
        sessionStorage.setItem('allSavedProduct', allSavedProduct.html());
    
        sessionStorage.setItem('data-counter', productCaretCounter);
    
        if (productCaretCounter === 0) {
            $('.nav-bar__right__menu__bag-product__content__product__empty').removeClass('d-none');
            $('.nav-bar__right__menu__bag-product__btn span').addClass('d-none');
        }
    
    
    });


    $('.nav-bar__right__menu__bag-product__content__product').append(sessionStorage.getItem('allSavedProduct'));
    $('.nav-bar__right__menu__bag-product__content__head__count').text(sessionStorage.getItem('data-counter'));







    if (productCaretCounter === 0) {
        $('.nav-bar__right__menu__bag-product__content__product__empty').removeClass('d-none');
    } else {
        $('.nav-bar__right__menu__bag-product__content__product__empty').addClass('d-none')
        $('.nav-bar__right__menu__bag-product__btn span').removeClass('d-none');
    }


    // check if totalPrice of localStorage is empety
    var checkPrice = sessionStorage.getItem('totalPrice');
    if (checkPrice == null) {
        var initialPrice = 0;
        $('.nav-bar__right__menu__bag-product__content__footer__info .total-price').text(initialPrice);
    }



    // function futeure products

    $(".general-sidebar__left__feature__content__main").slice(0,3).show();
    $('.general-sidebar__left__feature__head__next').click(function() {
        var items = $('.general-sidebar__left__feature__content__main:visible').hide().last();
	    
	    var nextItems = items.nextAll().slice(0, 3);
	    
	    if (nextItems.length === 0) {
	        nextItems = $(".general-sidebar__left__feature__content__main").slice(0, 3);
	    }
	    nextItems.fadeIn(500);
	    
    });

    $('.general-sidebar__left__feature__head__previous').click(function() {
        var items = $('.general-sidebar__left__feature__content__main:visible').hide().first();
	    
	    var nextItems = items.prevAll().slice(0, 3);
	    
	    if (nextItems.length === 0) {
	        nextItems = $(".general-sidebar__left__feature__content__main").slice(0, 3);
	    }
	    nextItems.fadeIn(500);
	    
    });


    // send ajax post to wishlist modal

    
    $(document).on('click', '.fashion__right .products-info .head-content .heart', function() {
        var inputVal = parseInt($(this).closest('.main-coulom').find('input').val());
        var productDataId = $(this).closest('.main-coulom').attr('id');
        var productCode = $(this).closest('.main-coulom').attr('data-code');
        var productColor = $(this).closest('.main-coulom').attr('data-color');
        var productTitle = $(this).closest('.main-coulom').find('.product-content__head').text();
        var productPrice = $(this).closest('.main-coulom').find('.product-content__price').text();
        var productImg = $(this).closest('.main-coulom').find('img:first').attr('src');


        var wishList = {
            productQty: inputVal,
            productId : productDataId,
            productCode : productCode,
            productColor : productColor,
            productTitle : productTitle,
            productPrice : productPrice,
            productImg : productImg,
        }


        $.ajax({
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            type: "POST",
            url: '/wish-list',
            data: wishList,
            success: function(wishList) {
                console.log(wishList);
            }
        });


    })


});
