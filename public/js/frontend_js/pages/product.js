$(document).ready(function () {
    $(".links__more").click(function(){
        $(".more__links-more").toggleClass('more-links-show')
    });
    $('.links__more').on('click', function(e) {
        e.preventDefault();
    });

    $(".all__rating__products").each(function () {
        // var rating = $(".all__rating span svg").length;
        var rating = $(this).find('svg').length;
        $(this).attr('data-original-title', rating + ".00");
        // $(this).attr('data-original-title', da);
    });


    // $("elmfrod aktb hena elklma ely htkon gwa title e7na kda brdo m3mlnash select leha").text(rating)



    $('.features').owlCarousel({
        // autoplay:true,
        rtl:true,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsMobile: false,
        loop: true,
        stagePadding: 0,
        margin: 50,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1024: {
                items: 4,
            },
            1260: {
                items: 5,
            }
        }
    });

    $('.slider-product').owlCarousel({
        items:1,
        loop:true,
        // rtl:true,
    });


    $('#product-item-first').zoom();
    $('#product-item-second').zoom();
    $('#product-item-third').zoom();
    $('#product-item-forth').zoom();

    $('.owl-dot ').click(function () {
        $(".owl-dot").addClass('owl-dot-cover')
    });

    $(".nav-tabs li").click(function () {
        $(this).addClass('active').siblings().removeClass('active')
    });

    $(".tags a").click(function () {
        $(".tags").addClass('tags-show');
        $(".artical-tab__content").addClass('description-hidden');
        $(".table").removeClass('table-show');
        $(".size").removeClass('size-show');
    });

    $(".Description a").click(function () {
        $(".artical-tab__content").removeClass('description-hidden');
        $(".tags").removeClass('tags-show');
        $(".table").removeClass('table-show');
        $(".size").removeClass('size-show');
    });

    $(".rview a").click(function () {
        $(".table").addClass('table-show');
        $(".artical-tab__content").addClass('description-hidden');
        $(".tags").removeClass('tags-show');
        $(".size").removeClass('size-show');
    });
    $(".guid-size a").click(function () {
        $(".size").addClass('size-show');
        $(".artical-tab__content").addClass('description-hidden');
        $(".table").removeClass('table-show');
        $(".tags").removeClass('tags-show');
    });

    $('.owl-products').owlCarousel({
        items: 1,
        loop: true,
        // rtl:true,

    });


    if ($(window).width() < 992) {
        $(".mini-sidebar").addClass("mini-sidebar-show");
        $(".body .sidebar-product.hidden").css('display', 'none')
    } else {
        $(".mini-sidebar").removeClass("mini-sidebar-show");
    }
    $(window).resize(function () {
        if ($(window).width() < 992) {
            $(".mini-sidebar").addClass("mini-sidebar-show");
        } else {
            $(".mini-sidebar").removeClass("mini-sidebar-show");
        }
    });

    $(".mini-sidebar-show").click(function () {
        $(".layout").addClass('layout-show');
        $(".translate-body .slide-left").addClass("slidebar-left-show");
        $('body').css('overflow', 'hidden');
    });

    $('.layout').click(function() {
        $(".translate-body .slide-left").removeClass("slidebar-left-show")
    })
    $('.nav-item a').on('click', function(e) {
        e.preventDefault();
    });

    $(".plus").click(function () {
        var test = $(this).siblings('input').val(); //y5od parent bta3 plus we b3dha ykon sibil6ing bta30 3shan a3ml select llinout
        test++;  //elinput value bt3tha tzed wa7d
        $(this).siblings('input').attr('value', test); // y3rd elkema elgdeda fy eldom w fy websit
    });

    $(".minus").click(function () {
        var test = $(this).siblings('input').val();
        if (test > 1) {
            test--;
            $(this).siblings('input').attr('value', test);
        }
    });

    const allProductItemsImgs = $('.product-slider-container .owl-item').not('.cloned').find('.product-item img:first-of-type');
    const owlDotBtns = $('.product-slider-container .owl-dots button');

    allProductItemsImgs.each(function(index){
        const imgSrc = allProductItemsImgs[index].src;
        owlDotBtns[index].style.backgroundImage = `url('${imgSrc}')`;
    });


    $('.product-content__star').each(function() {
        var svgRate = $(this).attr('data-star');
        var spanRate = $(this).find('span');

        for (i=0; i <svgRate;i++) {
            spanRate[i].classList.add('review-star');
        }
        
    });

    if ($('.product-single-detailes #selSize option').length <= 1) {
        $('.product-action').addClass('opacity-half');
        $('.product-single-detailes__message').removeClass('d-none');
    }


});

















