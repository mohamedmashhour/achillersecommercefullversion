



$(function() {
    // function for count products
    var counter = $(".dropdown .link__count");

    var childsParent = $(".dropdown .child .child__create");

    counter.text(childsParent.length);

    // function dropdown clicked
    $('.choices').on('click', function () {
        $('.selection').click();

    });

    $('.checkout-form1').validate({
        rules :{
            billing_name: {
                required: true,
                // minlength: 20,
            },
            billing_address: {
                required: true,
            },
            billing_city: {
                required: true,
            },
            billing_state : {
                required: true,
            },
            billing_country: {
                required: true,
            },
            billing_pincode: {
                required: true,
            },
            billing_mobile: {
                required: true,
                minlength: 11
            }
            
        },

    });

    $('.checkout-form2').validate({
        rules :{
            shipping_name: {
                required: true,
                // minlength: 20,
            },
            shipping_address: {
                required: true,
            },
            shipping_city: {
                required: true,
            },
            shipping_state : {
                required: true,
            },
            shipping_pincode: {
                required: true,
            },
            shipping_country: {
                required: true,
            },
            shipping_mobile: {
                required: true,
                minlength: 11
            }
            
        },

    });

});

