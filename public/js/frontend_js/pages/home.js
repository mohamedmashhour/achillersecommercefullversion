$(function() {

    $('.men-collection-container .men-collection:odd').addClass('reverse-content');

    $('.collection').owlCarousel({
        rtl:true,
        autoplay: false,
        items: 1,
        loop: true,
        margin: 50,
        dots: false,
        nav:true,
        responsive: {
            567: {
                items: 2,
            },
            768: {
                items: 2,
            },
            993: {
                items: 2,
            },
            1260: {
                items: 2,
            }
        }
    });
    $('.slider-quick-view').owlCarousel({
        // rtl:true,
        autoplay: false,
        items: 1,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsMobile: false,
        loop: true,
        stagePadding: 0,
        margin: 10,
    });


    $('.slider').owlCarousel({
        rtl:true,
        items:1,
        loop:true,
        
    });

    $('.features').owlCarousel({
        // autoplay:true,
        rtl:true,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsMobile: false,
        loop: true,
        stagePadding: 0,
        margin: 50,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1024: {
                items: 4,
            },
            
        }
    });



    $('.product-content__star').each(function() {
        var svgRate = $(this).attr('data-star');
        var spanRate = $(this).find('span');

        for (i=0; i <svgRate;i++) {
            spanRate[i].classList.add('review-star');
        }
        
    });



});

