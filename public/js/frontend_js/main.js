
$(document).ready(function () {
    //preload of pages
    $('body').addClass('overflow-hidden');
    $('.preload__container').fadeOut(200, function() {
        $('.preload').fadeOut(200, function() {
            $('body').removeClass('overflow-hidden');
        });
    });

    // functions sidebae of general page
    const sidebarLinks1 = $('a.sidebar-link-lv1');

    sidebarLinks1.click(function () {
        const thisLinkMenu = $(this).siblings();
        const linkSubmenus = $('.link-submenu-lv1');
        linkSubmenus.not(thisLinkMenu).collapse('hide');
    });

    const sidebarLinks2 = $('a.sidebar-link-lv2');

    sidebarLinks2.click(function () {
        const thisLinkMenu = $(this).siblings();
        const linkSubmenus = $('.link-submenu-lv2');
        linkSubmenus.not(thisLinkMenu).collapse('hide');
    });


    // function sidebar black
    const sidebarBlackLink1 = $('a.link-sidebar-black');

    sidebarBlackLink1.click(function () {
        const thisLinkMenu = $(this).siblings();
        const linkSubmenus = $('.black-submenu-lv1');
        linkSubmenus.not(thisLinkMenu).collapse('hide');
    });

    const sidebarBlackLink2 = $('a.link-sidebar-black2');

    sidebarBlackLink2.click(function () {
        const thisLinkMenu = $(this).siblings();
        const linkSubmenus = $('.black-submenu-lv2');
        linkSubmenus.not(thisLinkMenu).collapse('hide');
    });


    // function general select color
    
    $(document).on('click', 'label.label-color', function () {
        $(this).find('.fa-check').toggleClass('d-none');
    });

        

    $(document).on('click', '.label-size', function() {
        $(document).find('label.label-size').not($(this)).removeClass('set-size');
        $(this).toggleClass('set-size');
    });

    $('label.label-categories').click(function () {
        $(document).find('label.label-categories').not($(this)).removeClass('set-cat');
        $(this).toggleClass('set-cat');
    });




    $(document).on('click', '.general-sidebar__left__size input[type=radio]', function() {
        var previousValue = $(this).data('storedValue');
          if (previousValue) {
            $(this).prop('checked', !previousValue);
            $(this).data('storedValue', !previousValue);
          }
          else{
            $(this).data('storedValue', true);
            $(".general-sidebar__left__size input[type=radio]:not(:checked)").data("storedValue", false);
          }
    });

    
    $('.label-categories ~ input[type=radio]').on('click', function() {
        var previousValue = $(this).data('storedValue');
          if (previousValue) {
            $(this).prop('checked', !previousValue);
            $(this).data('storedValue', !previousValue);
          }
          else{
            $(this).data('storedValue', true);
            $(".label-categories ~ input[type=radio]:not(:checked)").data("storedValue", false);
          }
    });

    // check if childs of cat is exsist

    $('.link-submenu-lv1').each(function() {

        if ($(this).children().length == 0 ) {
            $(this).siblings().find('.fa-plus, .fa-minus').addClass('d-none');
            $(this).siblings().addClass('opacity-half');
       }

    })






    $('.head-link').on('click', function () {
        $(this).find('.angle-down').toggleClass('angle-rotate');
    });


    // function general search in all pages
    $(".right__search").click(function (e) {
        e.stopPropagation();
        $(".sec-level-search").toggleClass('sec-level-search__show');
        $('.right__search').toggleClass('show-arrow-search');
        $('.nav-bar__right__menu__bag-product').removeClass('show-product-content')
    });

    // function stop search from close
    $(".sec-level-search").click(function (e) {
        e.stopPropagation();
    });





    //function of bag shopping
    $('.nav-bar__right__menu__bag-product .nav-bar__right__menu__bag-product__btn').click(function() {
        $(this).closest('.nav-bar__right__menu__bag-product').toggleClass('show-product-content');
        $(".sec-level-search").removeClass('sec-level-search__show');
        $('.right__search').removeClass('show-arrow-search');
        
    });



    // function sticky topbar when scroll
    $(window).scroll(function () {
        var sc = $(this).scrollTop();
        if (sc > 10) {
            $("nav.nav-bar").addClass('sticky');
        } else {
            $("nav.nav-bar").removeClass('sticky');
        }
    });
    var sc = $(this).scrollTop();
    if (sc > 10) {
        $("nav.nav-bar").addClass('sticky');
    } else {
        $("nav.nav-bar").removeClass('sticky');
    }




    // function sidebar when click menu at responsive
    $("#menu").click(function () {
        $(".main-sidebar").addClass('move-overlay');
        $('body').addClass('overflow-hidden');
        $(".layout").addClass('layout-show');
        $(".sec-level-search").removeClass('sec-level-search__show');
        $('.right__search').removeClass('show-arrow-search');
    });

    $(".main-sidebar__close span").click(function () {
        $('body').removeClass('overflow-hidden');
        $('.layout').addClass('d-none').removeClass('layout-show');
        $(".main-sidebar").removeClass('move-overlay');
        
    });


    $('.layout').on('click', function () {
        $(".main-sidebar__close svg").click();
    });

    $('.general-sidebar__responsive-btn').on('click', function () {
        $(this).toggleClass('toggle-responsive-btn');
        $('.general-sidebar__left').toggleClass('reset-sidebar');
        $('.general-sidebar__overlay').toggleClass('d-none');
        $('.general-sidebar__responsive-btn .fa-sliders-h').toggleClass('d-none');
        $('.general-sidebar__responsive-btn .fa-times').toggleClass('d-none');
        $('body').toggleClass('overflow-hidden');
        $(".sec-level-search").removeClass('sec-level-search__show');
        $('.right__search').removeClass('show-arrow-search');
    });

    $('.general-sidebar__overlay').on('click', function () {
        $('.general-sidebar__responsive-btn').click();
    });


    // function general increase and dicrease value of input

    $(document).on('click', '.plus', function() {
        var inputlVal = $(this).siblings('form').find('input').val();
        inputlVal++;
        $(this).siblings('form').find('input').attr('value', inputlVal);
    });

    $(document).on('click', '.minus', function() {
        var inputlVal = $(this).siblings('form').find('input').val();
        if (inputlVal > 1) {
            inputlVal--;
            $(this).siblings('form').find('input').attr('value', inputlVal);
        }
    });


    // Scroll To Top
    var scrollToTop = $('.general-to-up');
    scrollToTop.click(function () {
        $("html, body").animate({ scrollTop: 0 }, 1000);
        return false;
    });

    // show button scroll to top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.general-to-up').fadeIn();
        } else {
            $('.general-to-up').fadeOut();
        }
    });

    if ($(this).scrollTop() > 100) {
        $('.general-to-up').fadeIn();
    } else {
        $('.general-to-up').fadeOut();
    }

    $('.general-add-new-item').click(function() {
        $('.modal .close').click();
    });


    // validation

    $('.general-form').validate({
        rules :{
            name: {
                required: true,
            },
            email : {
                required: true,
                email: true,
            }
        },
    });


    // login form
    $('#loginForm').validate({
        rules :{
            password__input: {
                required: true,
            },
            email__input : {
                required: true,
                email: true,
            }
        }

    });

    $('#registerForm').validate({
        rules :{
            name__input: {
                required: true,
            },
            password__input: {
                required: true,
            },
            email__input : {
                required: true,
                email: true,
            }
        }

    });

});
