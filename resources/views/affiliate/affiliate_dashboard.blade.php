@extends('layouts.frontLayout.front_design')
@section('content')
<main class="affiliate-acount">
    <!-- Start breadcrumb Of Page -->
    <div class="breadcrumb-nav">
        <div class="container-fluid pl-4">
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" title="Back to the frontpage"><img src="{{asset('images/home.svg')}}" alt="home"></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">affiliate login page</li>
            </ul>
        </nav>
        </div>
    </div>
    <!-- End breadcrumb Of Page -->

    <div class="affiliate-acount__header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-8 d-flex">
                    <div class="affiliate-acount__header__avatar">
                        <img src="{{asset('images/profile.jpg')}}" alt="img">
                    </div>
                    <div class="affiliate-acount__header__content">
                        <h1>mostafa adel</h1>
                        <h4>Affiliate since October 31, 2019 2:58 pm</h4>
                        <span>basic</span>
                    </div>
                </div>
                
                <div class="col-12 col-md-4 affiliate-acount__header__left-side">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-end affiliate-acount__header__left-side__content">
                                <div>
                                    <span>Referrals</span>
                                    <span>1</span>
                                </div>
                                <div>
                                    <span>Earnings</span>
                                    <span>29.9USD</span>
                                </div>
                            </div>
                            <div class="col-12 d-flex justify-content-end justify-content-end">
                                <div class="affiliate-acount__header__left-side__footer"></div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>

    <!-- Start Navbar Of Page -->
    <div class="container-fluid pl-4">
        <nav class="top-bar">
            <a class="top-bar__main-link" href="#">
                <i class="fas fa-home"></i> overview
            </a>
            <a class="top-bar__main-link" href="#">
                <i class="fas fa-angle-right"></i> profile
                <ul class="top-bar__main-link-hidden-menu">
                    <li>
                        <i class="fas fa-user-alt"></i> edit account
                    </li>
                    <li>
                        <i class="fas fa-lock-open"></i> change password
                    </li>
                    <li>
                        <i class="far fa-money-bill-alt"></i> payment setting
                    </li>
                </ul>
            </a>
            <a class="top-bar__main-link" href="#">
                <i class="fas fa-angle-right"></i> marketing
                <ul class="top-bar__main-link-hidden-menu">
                    <li>
                        <i class="fas fa-user-alt"></i> edit account
                    </li>
                    <li>
                        <i class="fas fa-lock-open"></i> change password
                    </li>
                    <li>
                        <i class="far fa-money-bill-alt"></i> payment setting
                    </li>
                </ul>
            </a>

            <a class="top-bar__main-link" href="#">referrals</a>

            <a class="top-bar__main-link" href="#">payments</a>

            <a class="top-bar__main-link" href="#">
                <i class="fas fa-angle-right"></i> overall
                <ul class="top-bar__main-link-hidden-menu">
                    <li>
                        <i class="fas fa-user-alt"></i> edit account
                    </li>
                    <li>
                        <i class="fas fa-lock-open"></i> change password
                    </li>
                    <li>
                        <i class="far fa-money-bill-alt"></i> payment setting
                    </li>
                </ul>
            </a>

            <a class="top-bar__main-link" href="#">help</a>
            <a class="top-bar__main-link" href="#">logOut</a>
        </nav>
    </div>
    <!-- End Navbar Of Page -->

    <!-- Start content of affilliate -->
    <div class="affiliate-acount__content">
        <div class="container-fluid">
            <h1 class="affiliate-acount__content__title">overview</h1>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-6">
                                <div class="affiliate-acount__content__info">
                                    <span>1</span>
                                    <span>Total Referrals</span>
                                </div>
                                
                            </div>
                            <div class="col-6">
                                <div class="affiliate-acount__content__info">
                                    <span>1</span>
                                    <span>Total Referrals</span>
                                </div>
                                
                            </div>
                            <div class="col-12">
                                <div class="affiliate-acount__content__main-info">
                                    <span>29.9USD</span>
                                    <span>Your Earnings by Now (total Transactions)</span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-12 col-lg-6">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-6">
                                <div class="affiliate-acount__content__info">
                                    <span>1</span>
                                    <span>Total Referrals</span>
                                </div>
                                
                            </div>
                            <div class="col-6">
                                <div class="affiliate-acount__content__info">
                                    <span>1</span>
                                    <span>Total Referrals</span>
                                </div>
                                
                            </div>
                            <div class="col-12">
                                <div class="affiliate-acount__content__main-info">
                                    <span>29.9USD</span>
                                    <span>Your Earnings by Now (total Transactions)</span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End content of affilliate -->

</main>
@endsection