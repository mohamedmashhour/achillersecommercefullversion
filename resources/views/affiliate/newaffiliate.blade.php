@extends('layouts.frontLayout.front_design')
@section('content')
<main class="affiliate__login">
    <!-- Start breadcrumb Of Page -->
    <div class="breadcrumb-nav">
        <div class="container-fluid pl-4">
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" title="Back to the frontpage"><img src="{{asset('images/home.svg')}}"
                    alt="home"></a>
            </li>

            

            <li class="breadcrumb-item active" aria-current="page">affiliate login page</li>
            </ul>
        </nav>
        </div>
    </div>
    <!-- End breadcrumb Of Page -->


    <div class="affiliate__login__content">
        <form class="general-form" method="POST" action="{{ url('/new-affiliate') }}">
            {{ csrf_field() }}
            <div class="container-fluid">
                <div class="row no-gutters">
                    <div class="col-12 mt-5">
                        <input type="text" name="name" placeholder="YourName">
                        <i class="fas fa-user"></i>
                    </div>
    
                    <div class="col-12 mt-4">
                        <input type="text" name="email" placeholder="Email">
                        <i class="fas fa-envelope"></i>
                    </div>
                    <div class="col-12 mt-4">
                      <input type="password" name="password" placeholder="Password">
                      <i class="fas fa-lock"></i>
                  </div>
                    <div class="col-12 mt-3 pl-3 text-center">
                        <button type="submit">sign up</button>
                    </div>

                    <div class="col-12 mt-3 mb-5 pl-3 text-center affiliate__login__content__footer">
                        <span>Have An Account?</span><a href="#">login</a>
                    </div>

                </div>
            </div>
            
        </form>
    </div>

</main>

@endsection