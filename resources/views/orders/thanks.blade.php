@extends('layouts.frontLayout.front_design')
@section('content')
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  <li><a href="/">Home</a></li>
			  <li class="active">Thanks</li>
			</ol>
		</div>
	</div>
</section>
<main class="thanks">
        
	<div class="container check-out">
		<div class="title">
			<p>Thank you.Your data has been recieved. </p>
		</div>
			<div class="data">
				<ul>
					<li>Order number: <span>{{ Session::get('order_id') }}</span></li>
					<li>Data:<span>1589</span></li>
					<li>Total:<span>{{ Session::get('grand_total') }}</span></li>
					<li>Payment method:<span>Cash on delivery</span></li>
				</ul>
			</div>
			<p class="payment"> pay with cash upon on delivery</p>
			<div class="details">
				<h1>
					Order details
				</h1>
				<div class="reciept-details">
					<table>
						<tr class="table-head">
							<th>product</th>
							<th>Total</th>
						</tr>
						<tr>
							<td>men jacket * 1</td>
							<td>EGP 299.00</td>
						</tr>
						<tr>
							<td>subtotal</td>
							<td>EGP 299.00</td>
						</tr>
						<tr>
							<td>shipping</td>
							<td>EGP 299.00 <span>via flat rate</span></td>
						</tr>
						<tr>
							<td>payment method</td>
							<td>cash on delivery</td>
						</tr>
						<tr>
							<td>total</td>
							<td>{{ Session::get('grand_total') }}</td>
						</tr>
					</table>
				</div>
			</div>
	</div>

</main>
<section id="do_action">
	<div class="container">
		<div class="heading" align="center">
			<h3>YOUR COD ORDER HAS BEEN PLACED</h3>
			<p>Your order number is {{ Session::get('order_id') }} and total payable about is INR {{ Session::get('grand_total') }}</p>
		</div>
	</div>
</section>

@endsection

<?php
Session::forget('grand_total');
Session::forget('order_id');


?>