@extends('layouts.frontLayout.front_design')
@section('content')

<div class="container checkout pt-4">
    <ul class="checkout__progress-bar">
        <li class="active">
            <span>Shipping</span>
        </li>
        <li>
            <span data-step="2">Review & Payments</span>
        </li>
    </ul>
    <!--data enter-->
    <div class="row products">
        <div class="col-lg-6">
            <ul class="checkout-steps">
                <li class="checkout-steps__data">
                    <h2>Shipping Address</h2>
                    <!--form-->
                    <form class="checkout-form1" action="{{ url('/checkout') }}" method="post">{{ csrf_field() }}
                        <div class="form form__email">
                            <label> Name </label>
                            <div class="email__input">
                                <input name="billing_name" id="billing_name" @if(!empty($userDetails->name))
                                value="{{ $userDetails->name }}" @endif type="text" placeholder="Billing Name"
                                class="form-control" />
                            </div>
                        </div>
                        <div class="form form__email">
                            <label> Address </label>
                            <div class="email__input">
                                <input name="billing_address" id="billing_address" @if(!empty($userDetails->address))
                                value="{{ $userDetails->address }}" @endif type="text" placeholder="Billing Address"
                                class="form-control" />
                            </div>
                        </div>
                        <div class="form form__email">
                            <label> City </label>
                            <div class="email__input">
                                <input name="billing_city" id="billing_city" @if(!empty($userDetails->city))
                                value="{{ $userDetails->city }}" @endif type="text" placeholder="Billing City"
                                class="form-control" />
                            </div>
                        </div>
                        <div class="form form__email">
                            <label> State </label>
                            <div class="email__input">
                                <input name="billing_state" id="billing_state" @if(!empty($userDetails->state))
                                value="{{ $userDetails->state }}" @endif type="text" placeholder="Billing State"
                                class="form-control" /> </div>
                        </div>
                        <div class="form form form__State">
                            <label> Country </label>
                            <div class="form-selection">
                                <select id="billing_country" name="billing_country" class="form-control selection">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                    <option value="{{ $country->country_name }}" @if(!empty($userDetails->country) &&
                                        $country->country_name == $userDetails->country) selected
                                        @endif>{{ $country->country_name }}</option>
                                    @endforeach
                                </select>
                                <div class="choices">
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form form__email">
                            <label> Pin Code </label>
                            <div class="email__input">
                                <input name="billing_pincode" id="billing_pincode" @if(!empty($userDetails->name))
                                value="{{ $userDetails->pincode }}" @endif type="text" placeholder="Billing Pincode"
                                class="form-control" />
                            </div>
                        </div>
                        <div class="form form__email">
                            <label> Mobile </label>
                            <div class="email__input">
                                <input name="billing_mobile" id="billing_mobile" @if(!empty($userDetails->mobile))
                                value="{{ $userDetails->mobile }}" @endif type="text" placeholder="Billing Mobile"
                                class="form-control" />
                            </div>
                        </div>
                        <div class="form__footer">
                            <button type="submit" class="btn btn-primary">next</button>
                        </div>
                </li>
            </ul>
        </div>
        <div class="col-lg-6">
            <ul class="checkout-steps">
                <li class="checkout-steps__data">
                    <h2>Billing to</h2>
                    <!--form-->

                    <from class="form-billing">

                    <div class="form name">
                        <label>Name </label>
                        <div class=" input name__input">
                            <input name="shipping_name" id="shipping_name" @if(!empty($shippingDetails->name))
                            value="{{ $shippingDetails->name }}" @endif type="text" placeholder="Shipping Name"
                            class="form-control" />
                        </div>
                    </div>

                    <div class=" input form form__street">
                        <label> Street Address </label>
                        <div class="input street__input">
                            <input name="shipping_address" id="shipping_address" @if(!empty($shippingDetails->address))
                            value="{{ $shippingDetails->address }}" @endif type="text" placeholder="Shipping Address"
                            class="form-control" />
                        </div>
                    </div>
                    <div class="form form__city">
                        <label>city </label>
                        <div class=" input city_input">
                            <input name="shipping_city" id="shipping_city" @if(!empty($shippingDetails->city))
                            value="{{ $shippingDetails->city }}" @endif type="text" placeholder="Shipping City"
                            class="form-control" />
                        </div>
                    </div>
                    <div class="form form__State">
                        <label>State/Province </label>
                        <div class="form-selection">
                            <input name="shipping_state" id="shipping_state" @if(!empty($shippingDetails->state))
                            value="{{ $shippingDetails->state }}" @endif type="text" placeholder="Shipping State"
                            class="form-control" />
                        </div>
                    </div>
                    <div class="form form__Zip">
                        <label>Zip/Postal Code </label>
                        <div class=" input Zip_input">
                            <input name="shipping_pincode" id="shipping_pincode" @if(!empty($shippingDetails->pincode))
                            value="{{ $shippingDetails->pincode }}" @endif type="text" placeholder="Shipping Pincode"
                            class="form-control" />
                        </div>
                    </div>
                    <div class="form form__State">
                        <label>country</label>
                        <div class="form-selection">
                            <select id="shipping_country" name="shipping_country" class="form-control">
                                <option value="">Select Country</option>
                                @foreach($countries as $country)
                                <option value="{{ $country->country_name }}" @if(!empty($shippingDetails->country) &&
                                    $country->country_name == $shippingDetails->country) selected
                                    @endif>{{ $country->country_name }}</option>
                                @endforeach
                            </select>
                            <div class="choices">
                                <i class="fas fa-chevron-down"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form form__phone">
                        <label>phone number </label>
                        <div class=" input phone_input">
                            <input name="shipping_mobile" id="shipping_mobile" @if(!empty($shippingDetails->mobile))
                            value="{{ $shippingDetails->mobile }}" @endif type="text" placeholder="Shipping Mobile"
                            class="form-control" />
                        </div>
                    </div>
                    

                    </form>
                </li>

            </ul>
        </div>

    </div>
</div>

@endsection
@push('scripts')
<script src="js/frontend_js/pages/checkout.js"></script>
@endpush