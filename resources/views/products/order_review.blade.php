@extends('layouts.frontLayout.front_design')
@section('content')
    <?php use App\Product; ?>
    <main class="whish-list">
        
        <div class="whish-list__info">
            <div class="container">
                <div class="row">
                @if(Session::has('flash_message_error'))
		            <div class="alert alert-error alert-block" style="background-color:#f4d2d2">
		                <button type="button" class="close" data-dismiss="alert">×</button> 
		                    <strong>{!! session('flash_message_error') !!}</strong>
		            </div>
                @endif 
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="whish-list__info__left">
                
                            <h1>Billing Details</h1>
                            <ul>
                                <li>{{ $userDetails->name }}</li>
                                <li>{{ $userDetails->address }}</li>
                                <li>{{ $userDetails->city }}</li>
                                <li>{{ $userDetails->state }}</li>
                                <li>{{ $userDetails->country }}</li>
                                <li>{{ $userDetails->pincode }}</li>
                                <li>{{ $userDetails->mobile }}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="whish-list__info__right">
                            <h1>Shipping Details</h1>
                            <ul>
                                <li>{{ $shippingDetails->name }}</li>
                                <li>{{ $shippingDetails->address }}</li>
                                <li>{{ $shippingDetails->city }}</li>
                                <li>{{ $shippingDetails->state }}/li>
                                <li>{{ $shippingDetails->country }}</li>
                                <li>{{ $shippingDetails->pincode }}</li>
                                <li>{{ $shippingDetails->mobile }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
        </div>
        
        <div class="wishlist__table-section">
            <div class="container">
                <table class="wishlist__table-section__table">
                    <thead class="wishlist__table-section__table__head">
                        <tr>
                            <th>item</th>
                            <th class="text-center">price</th>
                            <th class="text-center">quantity</th>
                            <th class="text-center">total</th>
                        </tr>
                    </thead>
                    <tbody class="wishlist__table-section__table__body">
                        <?php $total_amount = 0; ?>
						@foreach($userCart as $cart)
                        <tr>
                            <td class="wishlist__table-section__table__body__img">
                                <div><img src="{{ asset('/images/backend_images/product/small/'.$cart->image) }}" alt="d"></div>
                                <div>{{ $cart->product_name }}</div>
                            </td>
                            <?php $product_price = Product::getProductPrice($cart->product_id,$cart->size); ?>
                            <td class="text-center">LE{{ $product_price }}</td>
                            <td class="text-center">{{ $cart->quantity }}</td>
                            <td class="text-center">LE {{ $product_price*$cart->quantity }}</td>
                        </tr>
                        <?php $total_amount = $total_amount + ($product_price*$cart->quantity); ?>
						@endforeach
                    </tbody>
                </table>
            </div>
            
        </div>

        <div class="whish-list__product-info">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex justify-content-end">
                        <ul class="whish-list__product-info__cart">
                            <li class="whish-list__product-info__cart__list d-flex justify-content-between">
                                <div>cart sub total</div>
                                <div>{{ $total_amount }}</div>
                            </li>
                            <li class="whish-list__product-info__cart__list d-flex justify-content-between">
                                <div>shipping cart (+)</div>
                                <div>{{ $shippingCharges }}</div>
                            </li>
                            <li class="whish-list__product-info__cart__list d-flex justify-content-between">
                                <div>discount account (-)</div>
                                <div>
                                    @if(!empty(Session::get('CouponAmount')))
                                    LE {{ Session::get('CouponAmount') }}
                                @else
                                    LE 0
                                @endif</div>
                            </li>
                            <?php 
                            $grand_total = $total_amount + $shippingCharges - Session::get('CouponAmount');
                            $getCurrencyRates = Product::getCurrencyRates($total_amount); ?>
                            <li class="whish-list__product-info__cart__list d-flex justify-content-between">
                                <div>order total</div>
                                <div>
                                    <li><span class="btn-secondary" data-toggle="tooltip" data-html="true" title="
                                        USD {{ $getCurrencyRates['USD_Rate'] }}<br>
                                        GBP {{ $getCurrencyRates['GBP_Rate'] }}<br>
                                        EUR {{ $getCurrencyRates['EUR_Rate'] }}
                                        ">INR {{ $grand_total }}</span></li>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="whish-list__payment">
            <div class="container">
                <div class="whish-list__payment__content">
                    <div class="row">
                        <div class="col-12 col-md-6 d-flex pt-2 pb-2">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12 col-md-6 pt-2 pb-2">
                                        <span class="pr-3">select payment method:</span>
                                    </div>
                                    <div class="col-12 col-md-6 pt-2 pb-2">
                                        <span>
                                            <form name="paymentForm" id="paymentForm" action="{{ url('/place-order') }}" method="post">{{ csrf_field() }}
                                                <input type="hidden" name="grand_total" value="{{ $grand_total }}">
                                                @if($codpincodeCount>0)
                                                <div class="pr-3 d-flex align-items-center">
                                                    <label for="cdo">CDO</label>
                                                    <input id="cdo" type="radio" name="payment_method" value="COD">
                                                </div>
                                                @endif
					                            @if($prepaidpincodeCount>0)
                                                <div class="pr-3 d-flex align-items-center">
                                                    <label for="paypal">Paypal</label>
                                                    <input id="paypal" type="radio" name="payment_method" value="Paypal">
                                                </div>
                                                <div class="pr-3 d-flex align-items-center">
                                                    <label for="paymoney">Paymoney</label>
                                                    <input id="paymoney" type="radio" name="payment_method" value="Payumoney">
                                                </div>
                                                @endif
                                            
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                        <div class="col-12 col-md-6 d-flex justify-content-end pt-2 pb-2">
                            <div class="d-flex align-items-center">
                                <button type="submit" class="btn btn-default" onclick="return selectPaymentMethod();">Place Order</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

    </main>

@endsection
