<?php 
use App\Product; 
use App\Reviews;
use App\Template;
?>
@extends('layouts.frontLayout.front_design')
@section('content')



<div class="col-lg-3 sidebar-product slide-left">
    <div class="sidebar-right">
        <div class="last-sidebar">
            <div class="widget widget-brand">
                <a href="#">
                    <img src="images/product-brand.png">
                </a>
            </div>
            <div class="widget-info">
                <ul>
                    <li>
                        <i class="fas fa-shipping-fast"></i>
                        <h4>FREE<br>SHIPPING</h4>
                    </li>
                    <li>
                        <i class="fas fa-dollar-sign"></i>
                        <h4>100% MONEY<br>BACK GUARANTEE</h4>
                    </li>
                    <li><i class="fas fa-headset"></i><h4>ONLINE<br>SUPPORT 24/7</h4></li>
                </ul>
            </div>
            <div class="widget-banner">
                <div class="banner-img">
                    <a href="#">
                        <img src="images/banner-sidebar.jpg">
                    </a>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- Start breadcrumb Of Fashion Page -->
<div class="breadcrumb-nav">
    <div class="container-fluid pl-5">
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="/" title="Back to the frontpage"><img
                            src="{{asset('images/home.svg')}}" alt="home"></a>
                </li>
                <li class="breadcrumb-item"><a href={{url('/products/'.$categoryDetails->url)}}
                         title="Back to the frontpage">{{$categoryDetails->name}}</a>
                 </li>
                <li class="breadcrumb-item active" aria-current="page">{{$productDetails->product_name }}</li>
            </ul>
        </nav>
    </div>
</div>
<!-- End breadcrumb Of Fashion Page -->

<!-------------------- body  ------------------------>
<!-- owl carousel  -->
<div class="container-fluid body">
    <div class="row">

    <div class="col-12">
    @if(Session::has('flash_message_success'))
	            <div class="alert alert-success alert-block">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_success') !!}</strong>
	            </div>
	        @endif
			@if(Session::has('flash_message_error'))
	            <div class="alert alert-error alert-block" style="background-color:#d7efe5">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_error') !!}</strong>
	            </div>
            @endif 
    </div>
			
        <div class="col-lg-9 body__left-side">
            <div class="product">
                <form name="addtoCartForm" id="addtoCartForm" action="{{ url('add-cart') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="product_id" value="{{ $productDetails->id }}">
                    <input type="hidden" name="product_name" value="{{ $productDetails->product_name }}">
                    <input type="hidden" name="product_code" value="{{ $productDetails->product_code }}">
                    <input type="hidden" name="product_color" value="{{ $productDetails->product_color }}">
                    <input type="hidden" name="price" id="price" value="{{ $productDetails->price }}">
                    <div class="row">
                        <div class="col-lg-7 col-md-6 product-gallary">
                            <div class="product-slider-container ">
                                <div class="owl-carousel owl-theme owl-drag owl-loaded slider-product">

                                    @if(count($productAltImages)>0)
                                    @foreach($productAltImages as $altimg)
                                    <div class="product-item zoom" id='product-item-first'>
                                        <img src="{{ asset('images/backend_images/product/small/'.$altimg->image) }}">
                                        <!-- zoom  -->
                                    </div>
                                    @endforeach
                                    @else
                                    <img src="{{ asset('images/backend_images/product/small/'.$productDetails->image) }}">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 details">
                            <div class="product-single-detailes">
                                <h1>{{ $productDetails->product_name }}</h1>
                                <div class="rating">
                                    <div class="fonts">
                                <?php
                                    $product_rate = Reviews::where('product_id',$productDetails->id)->get();

                                    // dd($product_rate ->sum());
                                    // $rate = $product_rate->rate;
                                    $total_product_rate = $product_rate->sum('rate');
                                    $num_product_rate = $product_rate->count('rate');

                                    if($num_product_rate > 0){
                                        $product_rate = $total_product_rate / $num_product_rate;
                                        $product_rate = round($product_rate);
                                    }else{
                                        $product_rate = 0;
                                    }
                                     
                                ?>  
                                        
                                    </div>

                                    <div data-star="{{$product_rate}}" class="product-content__star">
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                    </div>
                                </div>
                                <div class="price-box">
                                    <span class="old-price">$81.00</span>
                                    <span class="new-price">{{ $productDetails->price }}</span>
                                </div>
                                <div class="product-desc">
                                    <p><?php echo nl2br($productDetails->description); ?></p>
                                </div>
                                <div class="product-desc">
                                    <p>not avilable</p>
                                </div>
                                <div class="colors">
                                    <h1>size:</h1>
                                    <div class="filter-color">
                                        <select id="selSize" name="size" style="width:150px;" required>
                                            <option value="0">Select</option>
                                            @foreach($productDetails->attributes as $sizes)
                                            <option value="{{ $productDetails->id }}-{{ $sizes->size }}">
                                                {{ $sizes->size }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="product-single-detailes__message pb-3 d-none">No size found</div>
                                <div class="product-action">
                                    <div class="product-qty">
                                        <div class="group">
                                            <span class="minus"><button type="button"></button></span>
                                            <input name="quantity" type="number" class="horizontal-quantity form-control"
                                                value="1">
                                            <span class="plus"><button type="button"></button></span>
                                        </div>
                                    </div>
                                    @if($total_stock>0)
                                    <button type="submit" class="btn btn-fefault cart add-carrt" id="cartButton"
                                        name="cartButton" value="Shopping Cart">
                                        <span> <i class="fas fa-shopping-bag"></i> Add to Cart</span>
                                    </button>
                                    @endif
                                    <button type="submit" class="span" id="WishListButton" name="wishListButton" value="Wish List"> <i class="fas fa-heart"></i>
                                        {{-- <span class="Add-Wishlis"> Add to Wishlist</span> --}}
                                    </button>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div>
                    @if(Session::has('flash_message_error'))
                    <div class="alert alert-error alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{!! session('flash_message_error') !!}</strong>
                    </div>
                    @endif   
                    @if(Session::has('flash_message_success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{!! session('flash_message_success') !!}</strong>
                        </div>
                    @endif
                </div>
                <div class="product-taps">
                    <ul class="nav nav-tabs">
                        <li class="nav-item Description active">
                            <a href="#artical-tab__content">Description</a>
                        </li>
                        <li class="nav-item rview">
                            <a href="#table">Reviews</a>
                        </li>
                        <li class="nav-item guid-size">
                            <a href="#size">Guid Size</a>
                        </li>
                    </ul>
                    <div class="artical-tab">
                        <div class="artical-tab__content" id="artical-tab__content">
                            <p><?php echo nl2br($productDetails->description); ?></p>
                            <ul>
                                <li><?php echo nl2br($productDetails->care); ?></li>
                            </ul>
                        </div>
                        

                        <div class="table" id="table">
                            <div class="product-reviews-content">
                                <div class="tittle">
                                    <ul>
                                        <li>Be the first to review this product</li>
                                    </ul>
                                </div>
                                <div class="add-product-review">
                                    <h3>WRITE YOUR OWN REVIEW</h3>
                                    <p>How do you rate this product? *</p>
                                    <form class="add-product-review__form" method="POST" action="{{url('/review')}}" >
                                    {{csrf_field()}}
                                        <div class="overflow-auto">
                                            <table class="table-rating">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>1 star</th>
                                                        <th>2 stars</th>
                                                        <th>3 stars</th>
                                                        <th>4 stars</th>
                                                        <th>5 stars</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="mean">Quality</td>
                                                        <td class="td-radio">
                                                            <div class="form-check">
                                                                <input class="form-check-input radio" type="radio"
                                                                    name="rate" value="1" id="ratingRadios1">
                                                                <label class="form-check-label" for="ratingRadios1">
                                                                    -
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td class="td-radio">
                                                            <div class="form-check">
                                                                <input class="form-check-input radio" type="radio"
                                                                    name="rate" value="2" id="ratingRadios2">
                                                                <label class="form-check-label" for="ratingRadios2">
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td class="td-radio">
                                                            <div class="form-check">
                                                                <input class="form-check-input radio" type="radio"
                                                                    name="rate" value="3" id="ratingRadios3">
                                                                <label class="form-check-label" for="ratingRadios3">
                                                                    -
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td class="td-radio">
                                                            <div class="form-check">
                                                                <input class="form-check-input radio" type="radio"
                                                                    name="rate" value="4" id="ratingRadios4">
                                                                <label class="form-check-label" for="ratingRadios4">
                                                                    -
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td class="td-radio">
                                                            <div class="form-check">
                                                                <input class="form-check-input radio" type="radio"
                                                                    name="rate" value="5" id="ratingRadios5">
                                                                <label class="form-check-label" for="ratingRadios5">
                                                                    -
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <input type="hidden" id="custId" name="product_id" value="{{$productDetails->id}}">
                                        <div class="form-group">
                                            <label>Your Email <span>*</span></label>
                                            <input type="text" name="user_email" required>
                                        </div>
                                        <div class="form-group message">
                                            <label>Comment of Your Review</label>
                                            <textarea class="comment" name="comment"></textarea>
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Submit Review">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="size  d-none" id="size">
                            <div class="row">
                                <div class="img col-12">
                                    <?php $size = Template::where(['key'=>'product_size_guide'])->get()?>
                                     @foreach ($size as $item)
                                        <img src="{{asset('images/backend_images/'.$item->value)}}">
                                    @endforeach
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- sidebar product  -->
        <div class="mini-sidebar"><i class="fas fa-sliders-h"></i></div>
        <div class="col-lg-3 sidebar-product hidden">
            <div class="sidebar-right">
                <div class="last-sidebar">
                    <div class="widget widget-brand">
                        <?php $pro1 = Template::where(['key'=>'product_promotion1'])->get()?>
                         @foreach ($pro1 as $item)
                        <a href="#">
                            <img src="{{asset('images/backend_images/'.$item->value)}}">
                        </a>
                        @endforeach
                    </div>
                    <div class="widget-info">
                        <ul>
                            <li>
                                <i class="fas fa-shipping-fast"></i>
                                <h4>FREE<br>SHIPPING</h4>
                            </li>
                            <li>
                                <i class="fas fa-dollar-sign"></i>
                                <h4>100% MONEY<br>BACK GUARANTEE</h4>
                            </li>
                            <li><i class="fas fa-headset"></i>
                                <h4>ONLINE<br>SUPPORT 24/7</h4>
                            </li>
                        </ul>
                    </div>
                    <div class="widget-banner">
                        <div class="banner-img">
                            <?php $pro2 = Template::where(['key'=>'product_promotion2'])->get()?>
                            @foreach ($pro2 as $item)
                            <a href="#" target="_blank">
                                <img src="{{asset('images/backend_images/'.$item->value)}}">
                            </a>
                            @endforeach
                        </div>
                    </div>
                    <!-- owl carousel  -->
                    <div class="widget widget-featured">
                        <h3 class="widget-title">
                            Featured Products
                        </h3>
                        <div class="widget-body">
                            <div class="owl-carousel owl-theme owl-drag owl-loaded owl-products">
                                <div class="featured-col">
                                    <!-- product widget  -->
                                    <div class="product-default left-details product-widget">
                                        <figure>
                                            <a href="#">
                                                <img src="img/product-1 (4).jpg">
                                            </a>
                                        </figure>
                                        <div class="product-details">
                                            <h2 class="product-title">
                                                <a href="#">Product Short Name</a>
                                            </h2>
                                            <div class="ratings-container">
                                                <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="product-price">$49.00</span>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- 2nd-->
                                    <div class="product-default left-details product-widget">
                                        <figure>
                                            <a href="#">
                                                <img src="img/product-2 (4).jpg">
                                            </a>
                                        </figure>
                                        <div class="product-details">
                                            <h2 class="product-title">
                                                <a href="#">Product Short Name</a>
                                            </h2>
                                            <div class="ratings-container">
                                                <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="product-price">$49.00</span>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- 3th -->
                                    <div class="product-default left-details product-widget">
                                        <figure>
                                            <a href="#">
                                                <img src="img/product-3 (3).jpg">
                                            </a>
                                        </figure>
                                        <div class="product-details">
                                            <h2 class="product-title">
                                                <a href="#">Product Short Name</a>
                                            </h2>
                                            <div class="ratings-container">
                                                <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="product-price">$49.00</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- 2nd part  -->
                                <div class="featured-col">
                                    <!-- product widget  -->
                                    <div class="product-default left-details product-widget">
                                        <figure>
                                            <a href="#">
                                                <img src="img/product-4 (3).jpg">
                                            </a>
                                        </figure>
                                        <div class="product-details">
                                            <h2 class="product-title">
                                                <a href="#">Product Short Name</a>
                                            </h2>
                                            <div class="ratings-container">
                                                <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="product-price">$49.00</span>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- 2nd-->
                                    <div class="product-default left-details product-widget">
                                        <figure>
                                            <a href="#">
                                                <img src="img/product-5 (1).jpg">
                                            </a>
                                        </figure>
                                        <div class="product-details">
                                            <h2 class="product-title">
                                                <a href="#">Product Short Name</a>
                                            </h2>
                                            <div class="ratings-container">
                                                <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="product-price">$49.00</span>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- 3th -->
                                    <div class="product-default left-details product-widget">
                                        <figure>
                                            <a href="#">
                                                <img src="img/product-6 (1).jpg">
                                            </a>
                                        </figure>
                                        <div class="product-details">
                                            <h2 class="product-title">
                                                <a href="#">Product Short Name</a>
                                            </h2>
                                            <div class="ratings-container">
                                                <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i> <i class="fas fa-star"></i> <i
                                                    class="fas fa-star"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="product-price">$49.00</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slide left sidebar product  -->
    </div>
</div>
<!-- sec owl carousel features -->
<div class="project" id="project">
    <div class="container-fluid">
        <div class="define">
            <h2>Featured Products</h2>
        </div>
        <div class="container-fluid">
            <div class="owl-carousel owl-theme owl-drag owl-loaded features">
                @foreach($productFeature as $product)
                <div class="all-product main-coulom">
                    <div class="producte ">


                    
                    <div class="product-content__info__hidden-colors d-none">
                        <span style="background-color:#{{$product->product_color}}" data-color="{{$product->product_color}}"></span>
                    </div>
                    
                    <div class="product-content__img d-none">
                        <img src="{{ asset('/images/backend_images/product/small/'.$product->image) }}">
                    </div>
                    <a href="{{ url('/product/'.$product->id) }}" class="img">
                        <img src="{{ asset('/images/backend_images/product/small/'.$product->image) }}">
                    </a>
                    <a class="main-layout" href=".quick-view" data-toggle="modal" data-target=".quick-view">quick
                        view</a>
                    <span>-20%</span>
                    <!-- modal bootstarp  -->
                    <!--<span class="logo" data-id="test1" data-src="img/product-1.jpg" data-content="jeans"
                                data-toggle="modal" data-target="#exampleModal">
                            </span>-->
                </div>
                <div class="all">
                    <div class="carousel-item-left">
                        <span class="office"><a href="#">{{$product->category_name}}</a></span>
                        <h6><a class="product-content__head" href="#">{{$product->product_name}}</a></h6>
                        <div class="all__rating">
                            <?php
                                $product_rate = Reviews::where('product_id',$product->id)->get();

                                // dd($product_rate ->sum());
                                // $rate = $product_rate->rate;
                                $total_product_rate = $product_rate->sum('rate');
                                $num_product_rate = $product_rate->count('rate');

                                if($num_product_rate > 0){
                                    $product_rate = $total_product_rate / $num_product_rate;
                                    $product_rate = round($product_rate);
                                }else{
                                    $product_rate = 0;
                                }
                            ?>
                            <div data-star="{{$product_rate}}" class="product-content__star">
                                <span><i class="fas fa-fw fa-star"></i></span>
                                <span><i class="fas fa-fw fa-star"></i></span>
                                <span><i class="fas fa-fw fa-star"></i></span>
                                <span><i class="fas fa-fw fa-star"></i></span>
                                <span><i class="fas fa-fw fa-star"></i></span>
                            </div>
                        </div>
                        <div class="price">
                            <span class="old-price">$90 </span>
                            <span class="new-price product-content__price">{{$product->price}}</span>
                        </div>
                    </div>
                    <div class="heart">
                        <a href="/product/{{$product->id}}"><i class="far fa-heart"></i></a>
                    </div>
                </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>


<!-- Start quck view modal of page -->
<div class="modal fade quick-view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="quick-view__product">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="pb-4 col-12 col-lg-6">
                                <div class="background-right-qickview"></div>

                                <div class="exzoom" id="exzoom">

                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="quick-view__product__right">
                                    <div class="row no-gutters">
                                        <div class="col-12">
                                            <a href="#" class="head">DAILY DEAL, FASHION</a>
                                        </div>
                                        <div class="col-12">
                                            <div class="star">
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                            </div>
                                            <span class="quick-view__product__right__no-review">no reviews</span>
                                        </div>
                                        <div class="col-12">
                                            <div class="par">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione
                                                laborum suscipit, soluta
                                                ullam non sed fugit expedita est ab consectetur enim illo qui in
                                                aperiam animi doloribus
                                                nostrum doloremque fugiat.
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="price">
                                                <span>$80.00</span>
                                            </div>
                                            <div class="details">

                                            </div>
                                        </div>
                                        <div class="col-12 size">
                                            <div class="row no-gutters mt-4 mb-3">
                                                <div class="col-2 d-flex align-items-center">
                                                    <h1>size: </h1>
                                                </div>
                                                <div class="col-10">
                                                    <div class="general-sidebar__left__size">
                                                        <form action="" class="p-0 d-flex">
                                                            <div>
                                                                <label for="med-size" class="label-size">m</label>
                                                                <input id="med-size" type="checkbox">
                                                            </div>
                                                            <div>
                                                                <label for="large-size" class="label-size">l</label>
                                                                <input id="large-size" type="checkbox">
                                                            </div>
                                                            <div>
                                                                <label for="xla-size" class="label-size">xl</label>
                                                                <input id="xla-size" type="checkbox">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 colors">
                                            <div class="row">
                                                <div class="col-2">
                                                    <h1>colors</h1>
                                                </div>
                                                <div class="col-10">
                                                    <div class="mb-3 general-sidebar__left__colors">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 pt-3 pb-3">
                                            <div class="quick-view__product__right__selected-items">
                                                <h1>selected items :</h1>
                                                <span class="quick-view__product__right__selected-items__qty"></span>
                                            </div>
                                        </div>

                                        <div class="col-12 add-product">
                                            <button class="add-product__add-cart" type="button">
                                                <span>add to cart</span>
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="../js/frontend_js/pages/product.js"></script>
@endpush