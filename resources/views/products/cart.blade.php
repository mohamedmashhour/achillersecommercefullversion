@extends('layouts.frontLayout.front_design')
@section('content')
<?php use App\Product; ?>
<div class="products">
    <div class="container">
        <div class="row">
            <!-- cart container -->
            <div class="col-lg-8 order-2 order-lg-1">
                <div class="all-table">
                    <div class="cart-table-cotainer head">
                        <div class="product">
                            <h6>product</h6>
                        </div>
                        <div class="price">
                            <h6>price</h6>
                        </div>
                        <div class="qty">
                            <h6>qty</h6>
                        </div>
                        <div class="total">
                            <h6>subtotal</h6>
                        </div>
                    </div>
                    <div class="body">
                        <?php $total_amount = 0; ?>
                        @foreach($userCart as $cart)
                        <div class="first-body">
                            <div class="first-body__describtion">
                                <div class="product">
                                    <div class=" img">
                                        <img src="{{ asset('/images/backend_images/product/small/'.$cart->image) }}">
                                    </div>
                                    <div class="product-title">
                                        <h2><a href="#">{{ $cart->product_name }}</a></h2>
                                    </div>
                                </div>
                                <div class="price">
                                    <?php $product_price = Product::getProductPrice($cart->product_id,$cart->size); ?>
                                    <p>{{ $product_price }}</p>
                                </div>
                                <div class="qty">
                                    <input type="text" class="input" id="input" value="{{ $cart->quantity }}">
                                    <div class="buttons">
                                        <button  class="plus"><i class="fas fa-sort-up"></i></button>
{{--                                        @if($cart->quantity>1)--}}
                                            <button class="minus"><i class="fas fa-sort-down"></i></button>
{{--                                        @endif--}}
                                    </div>
                                </div>
                                <div class="total">
                                    <p> {{ $product_price*$cart->quantity }}</p>
                                </div>
                            </div>
                            <div class="body__footer">
                                <div>
                                    <a href="#">Move to Wishlist</a>
                                </div>
                                <div class="fonts">
                                    <a href="{{ url('/cart/delete-product/'.$cart->id) }}"> <span><i class="fas fa-times"></i></span>
                                        <span class="remove">remove product</span></a>
                                </div>
                            </div>
                        </div>
                        <?php $total_amount = $total_amount + ($product_price*$cart->quantity); ?>
                        @endforeach
                    </div>
                    <div class="footer">
                        <div class="buttons">
                            <div class="left-button">
                            <a href="{{url('/shop')}}" class="btn">continue shopping</a>
                            </div>
                            <div class="right-button">
                                <a href="" class="btn">clear shopping card</a>
                                <a href="" class="btn update">update shopping card</a>
                            </div>
                        </div>
                        <div class="discount">
                            <h4>Apply Discount Code</h4>
                            <div class="input">
                                <form action="{{ url('cart/apply-coupon') }}" method="post">{{ csrf_field() }}
                                    <input type="text" name="coupon_code" placeholder="Enter discount code">
                                    <button type="submit" class="btn">Apply Discount</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end cart Container -->
            <!-- Summary Container-->
            <div class="col-lg-4 order-1 order-lg-2 Summary">
                <div class="cart">
                    <h3>Summary</h3>
                    <div class="dropdown">
                        <h4>
                            <a class=" link summary__link collapsed" data-toggle="collapse"
                                href="#multiCollapseExample10" role="button" aria-expanded="false"
                                aria-controls="multiCollapseExample10">
                                <span> estimate shiping and tax </span>
                                <i class="fas fa-angle-down"></i></a>
                        </h4>
                        <div class="row">
                            <div class="col">
                                <div class="collapse multi-collapse" id="multiCollapseExample10">
                                    <div class="card card-body body summary__body">
                                        <div class="child ">
                                            <p>Country</p>
                                            <form>
                                                <select class="select">
                                                    <option value="United State">United State</option>
                                                    <option value="Turkey">Turkey</option>
                                                    <option value="China">China</option>
                                                    <option value="Germany">Germany</option>
                                                </select>
                                            <p>State/Province</p>
                                                <select class="select">
                                                    <option value="calefornia">Calefornia</option>
                                                    <option value="taxes">Taxes</option>
                                                </select>
                                                <P>Zip/Postal Code</P>
                                                <input type="text" class="select">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- prices  -->
                    @if(!empty(Session::get('CouponAmount')))
                    <div class="prices">
                        <div class="word">
                            Sub Total
                        </div>
                        <div class="price">
                            <?php echo $total_amount; ?>
                        </div>
                    </div>
                    <div class="prices taxes">
                        <div class="word">Coupon Discount</div>
                        <div class="price"><?php echo Session::get('CouponAmount'); ?></div>
                    </div>
                    <div class="prices total">
                        <div class="word">Order Total</div>
                        <?php $total_amount = $total_amount - Session::get('CouponAmount');?>
                        <div class="price"><?php echo $total_amount; ?></div>
                        @else
                            <div class="prices">
                                <div class="word">
                                    Sub Total
                                </div>
                                <div class="price">
                                    <?php echo $total_amount; ?>
                                </div>
                            </div>
                            <div class="prices taxes">
                                <div class="word">Coupon Discount</div>
                                <div class="price">0</div>
                            </div>
                            <div class="prices total">
                                <div class="word">Order Total</div>
                                <div class="price"><?php echo $total_amount; ?></div>
                        @endif
                    </div>
                    <div class="check-out">
                        <a href="{{ url('/checkout') }}">GO TO CHECKOUT</a>
                    </div>
                </div>
            </div>
            <!-- end Summary Container-->
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="js/frontend_js/pages/shop.js"></script>
@endpush
