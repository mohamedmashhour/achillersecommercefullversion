<?php 
use App\Product;
use App\Reviews; 
?>
<div class="general-sidebar__left">
    <ul class="list-unstyled nice">
        <li>
            <a class="sidebar-link collapsed" href="#categories" role="button" id="sidebarLink" data-toggle="collapse"
                aria-haspopup="true" aria-expanded="false">
                <span>categories</span>
                <img src="{{asset('images/arrow.svg')}}" alt="img">
            </a>
            <ul id="categories" class="collapse link-submenu pb-3" aria-labelledby="sidebarLink">
                <ul>
                    @foreach($categories as $cat)
                    <li>
                        <a class="sidebar-link-lv1 collapsed" href="#cat{{$cat->id}}" role="button" id="sidebarLink"
                            data-toggle="collapse" aria-haspopup="true" aria-expanded="false">
                            <span>{{$cat->name}}</span>
                            <i class="fas fa-plus"></i>
                            <i class="fas fa-minus"></i>
                        </a>
                        <ul id="cat{{$cat->id}}" class="collapse link-submenu-lv1" aria-labelledby="sidebarLink">
                            @foreach($cat->categories as $subcat)
                                <?php $productCount = Product::productCount($subcat->id); ?>
                                @if($subcat->status==1)
                                @if($productCount>0)
                                <li>
                                    <div class="cat-links">
                                        <label class="label-categories" for="{{$subcat->id}}">{{$subcat->name}}</label>
                                        <input name="check-cat" id="{{$subcat->id}}" type="radio">
                                    </div>
                                    <span class="count-cat">{{ $productCount }}</span>
                                </li>
                                
                                @endif
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    @endforeach  
                </ul>
            </ul>
        </li>
        <li>
            <a class="sidebar-link collapsed" href="#prices" role="button" id="sidebarLink" data-toggle="collapse"
                aria-haspopup="true" aria-expanded="false">

                <span>prices</span>
                <img src="{{asset('images/arrow.svg')}}" alt="img">


            </a>
            <ul id="prices" class="collapse link-submenu" aria-labelledby="sidebarLink">
                <div class="general-sidebar__left__price">
                    <div class="general-sidebar__left__price__container">
                        <div class="container-fluid pl-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group max-price-select">
                                        <button type="button"><i class="fas fa-search"></i></button>
                                        <input placeholder="EX: 100 EG" type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </ul>
        </li>
        <li>
            <a class="sidebar-link collapsed" href="#colors" role="button" id="sidebarLink" data-toggle="collapse"
                aria-haspopup="true" aria-expanded="false">

                <span>colors</span>
                <img src="{{asset('images/arrow.svg')}}" alt="img">
            </a>
            <ul id="colors" class="collapse link-submenu" aria-labelledby="sidebarLink">
                <div class="general-sidebar__left__colors">
                    <form action="">
                        <div class="container p-0">
                            <div class="row no-gutters general-sidebar__left__color__content">
                                
                            </div>
                        </div>
                    </form>
                </div>
            </ul>
        </li>
        <li>
            <a class="sidebar-link collapsed" href="#size" role="button" id="sidebarLink" data-toggle="collapse"
                aria-haspopup="true" aria-expanded="false">

                <span>size</span>
                <img src="{{asset('images/arrow.svg')}}" alt="img">
            </a>
            <ul id="size" class="collapse link-submenu" aria-labelledby="sidebarLink">
                <div class="general-sidebar__left__size">
                    <form action="">
                        <div class="d-flex flex-wrap general-sidebar__left__size__content">
                            
                        

                        </div>
                    </form>
                </div>
            </ul>
        </li>
    </ul>
    <!-- side left bar  -->
    <!-- owl carousel  -->
    <div class="general-sidebar__left__feature pt-4">
        <div class="general-sidebar__left__feature__head">
            <div class="d-flex justify-content-between align-items-center">
                <h1>featred</h1>
                <div>
                    <button type="button" class="general-sidebar__left__feature__head__previous"></button>
                    <button type="button" class="general-sidebar__left__feature__head__next"></button>
                </div>
            </div>
        </div>
        <div class="general-sidebar__left__feature__content"> 
            <?php $featProducts = Product::inRandomOrder()->where('status',1)->where('feature_item',1)->paginate(12);?>   
            @foreach ($featProducts as $pro)      
                <div class="general-sidebar__left__feature__content__main">
                    <div class="container-fluid p-0">
                        <div class="row">
                            <div class="col-4 general-sidebar__left__feature__content__main__left">
                                <div>
                                    <img src="{{ asset('images/backend_images/product/small/'.$pro->image) }}" alt="img">
                                </div>
                            </div>
                            <div class="col-8 general-sidebar__left__feature__content__main__right">
                                <h4 class="head">{{$pro->product_name}}</h4>
                                <div class="star">
                                <?php
                                    $product_rate = Reviews::where('product_id',$pro->id)->get();

                                    // dd($product_rate ->sum());
                                    // $rate = $product_rate->rate;
                                    $total_product_rate = $product_rate->sum('rate');
                                    $num_product_rate = $product_rate->count('rate');

                                    if($num_product_rate > 0){
                                        $product_rate = $total_product_rate / $num_product_rate;
                                        $product_rate = round($product_rate);
                                    }else{
                                        $product_rate = 0;
                                    }
                                ?> 
                                    <span data-rate="{{$product_rate}}"></span>
                                    <i class="fas fa-star fa-fw"></i>
                                    <i class="fas fa-star fa-fw"></i>
                                    <i class="fas fa-star fa-fw"></i>
                                    <i class="fas fa-star fa-fw"></i>
                                    <i class="fas fa-star fa-fw"></i>
                                </div>
                                <div class="price">
                                    <span>LE{{$pro->price}}</span>
                                    <!--<span><del>$300</del></span>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach       
        </div>
    </div>
    <!-- /////////////////////////////////////////////////////////////////////////// -->
    <!--<div class="general-sidebar__left__custom">
        <h1>custom html block</h1>
        <h4>this a custom sub-title.</h4>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit debitis laudantium
            reiciendis ipsum
            quia sed omnis quaerat natus, veniam, commodi ipsa nesciunt culpa magnam pariatur!
            Mollitia asperiores
            in voluptas aliquid.</p>
    </div>-->
</div>