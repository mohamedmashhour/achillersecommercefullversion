<!DOCTYPE html>
<?php
if (!function_exists('lang')) {
function lang() {
if(session()->has('lang')){
return session('lang');
}else{
return 'en';
}

}
}

if (!function_exists('dirs')) {
function dirs() {
if(session()->has('lang')){
if(session('lang') == 'ar'){
return 'rtl';
}elseif(session('lang') == 'en'){
return 'ltr';
}
}else{
return 'ltr';
}

}
}
?>
<html lang="{{ lang()}}" dir="{{dirs()}}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@if(!empty($meta_title)){{ $meta_title }} @else Home | E-Shopper @endif</title>
        @if(!empty($meta_description))<meta name="description" content="{{ $meta_description }}">@endif
        @if(!empty($meta_keywords))<meta name="keywords" content="{{ $meta_keywords }}">@endif

        <link rel="icon" href="images/logo_2.png" type="image/gif" sizes="16x16">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="{{ asset('css/frontend_css/vendor/all.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/frontend_css/vendor/jquery.exzoom.css')}}">

        <link rel="stylesheet" href="{{ asset('css/frontend_css/vendor/bootstrap.min.css')}}">
        <!-- <link rel="stylesheet" href="css/vendor/screen.css"> -->
        <link rel="stylesheet" href="{{ asset('css/frontend_css/vendor/owl.carousel.min.css')}}">
        <Link rel="stylesheet" href="{{ asset('css/frontend_css/vendor/owl.theme.default.min.css')}}"/>
        <!-- <link rel="stylesheet" href="css/vendor/bootstraprtl.min.css"> -->
        <link rel="stylesheet" href="{{ asset('css/frontend_css/main-'.dirs().'.css')}}">
        <!-- <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5cc08788f3971d0012e248e5&product=inline-share-buttons' async='async'></script> -->
        <!-- <link rel="stylesheet" href="css/main-rtl.css"> -->
    </head>

    <body>
        @include('layouts.frontLayout.front_header')

        @yield('content')

        @include('layouts.frontLayout.front_footer')

        @yield('external')
          <!-- Start to Up button -->
            <div class="general-to-up">
                <span></span>
            </div>
            <!-- End to Up button -->

            <!-- Start Preloading Of Page -->
            <div class="preload">
                <section class="preload__container">
                    <div class='sk-three-bounce'>
                        <div class='sk-bounce-1 sk-child'></div>
                        <div class='sk-bounce-2 sk-child'></div>
                        <div class='sk-bounce-3 sk-child'></div>
                    </div>
                </section>
            </div>
            <!-- End Preloading Of Page -->
        <!-- <script src="{{ asset('js/frontend_js/vendor/jquery-3.3.1.min.js')}}"></script>
        <script src="{{ asset('js/frontend_js/vendor/popper.min.js')}}"></script>
        <script src="{{ asset('js/frontend_js/vendor/bootstrap.min.js')}}"></script> -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

        <script src="{{ asset('js/frontend_js/vendor/all.min.j')}}s"></script>

        <script src="{{ asset('js/frontend_js/vendor/jquery.validate.min.js')}}"></script>
        <script src="{{ asset('js/frontend_js/vendor/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('js/frontend_js/vendor/jquery.zoom.min.js')}}"></script>
        <script src="{{ asset('js/frontend_js/main.js')}}"></script>
        <script src="{{ asset('js/frontend_js/pages/fashion.js')}}"></script>
        @stack('scripts')
    </body>

</html>
