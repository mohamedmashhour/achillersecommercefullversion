<?php

use App\Http\Controllers\Controller;
use App\Product;
use App\Template;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;


$mainCategories = Controller::mainCategories();
$cartCount = Product::cartCount();

?>
<div class="translate-body">

    <!-- Start Sidebar of TopBar -->
    <div class="main-sidebar">
        <div class="main-sidebar__close">
            <span class="p-2">
                <i class="fas fa-times"></i>
            </span>
        </div>
        <!-- first level  -->
        <ul class="elements">
            <li class="elements__home">
                <a href="#" class="link">home</a>
            </li>
            <li class="elements elements__category">
                <a class="link-sidebar-black link category__link collapsed" data-toggle="collapse"
                    href="#multiCollapseExample1" role="button" aria-expanded="false"
                    aria-controls="multiCollapseExample1">
                    <span>category</span> <i class="fas fa-angle-down"></i>
                </a>
                <div class="black-submenu-lv1 collapse multi-collapse" id="multiCollapseExample1">
                    <div class="card card-body body category__body">
                        <ul class="sec-level">
                            <li><a href="#">Fullwidth Banner </a><span></li>
                            <li><a href="#">Boxed Slider Banner</a></li>
                            <li><a href="#">Boxed Image Banner</a></li>
                            <li><a href="#">Left Sidebar</a></li>
                            <li><a href="#">Right Sidebar</a></li>
                            <li><a href="#">Product Flex Grid</a></li>
                            <li><a href="#">Horizontal Filter1</a></li>
                            <li><a href="#">Horizontal Filter2</a></li>
                            <li><a href="#">PRODUCT LIST ITEMS TYPES</a></li>
                            <li><a href="#">AJECX INFINTY SCROLL</a> <span>New!</span></li>
                            <li><a href="#">Boxed Image Banner</a></li>
                            <li><a href="#">Left Sidebar</a></li>
                            <li><a href="#">Right Sidebar</a></li>
                            <li><a href="#">Product Flex Grid</a></li>
                            <li><a href="#">Horizontal Filter1</a></li>
                            <li><a href="#">Horizontal Filter2</a></li>
                        </ul>
                    </div>
                </div>
            </li>


            <li class="elements elements__product">
                <a class="link-sidebar-black link product__link collapsed" data-toggle="collapse"
                    href="#multiCollapseExample2" role="button" aria-expanded="false"
                    aria-controls="multiCollapseExample2">
                    <span>product</span> <i class="fas fa-angle-down"></i>
                </a>
                <div class="black-submenu-lv1 collapse multi-collapse" id="multiCollapseExample2">
                    <div class="card card-body body product__body">

                        <ul class="sec-level">


                            <li>
                                <a class="link-sidebar-black2 link product__link collapsed" data-toggle="collapse"
                                    href="#multiCollapseExample3" role="button" aria-expanded="false"
                                    aria-controls="multiCollapseExample3">
                                    <span>Variations</span> <i class="fas fa-angle-down"></i>
                                </a>
                                <div class="black-submenu-lv2 collapse multi-collapse" id="multiCollapseExample3">
                                    <div class="card card-body body product__body">
                                        <ul class="third-level">
                                            <li><a href="#"> Horizontal thumbnails</a></li>
                                            <li><a href="#"> vertical thumbnails</a><span>hot!</span></li>
                                            <li><a href="#"> inner zoom</a></li>
                                            <li><a href="#">addtocart sticky</a></li>
                                            <li><a href="#">accordion tabs</a></li>
                                        </ul>
                                    </div>
                                </div>


                            </li>



                            <li>
                                <a class="link-sidebar-black2 link product__link collapsed" data-toggle="collapse"
                                    href="#multiCollapseExample4" role="button" aria-expanded="false"
                                    aria-controls="multiCollapseExample4">
                                    <span>Variations</span> <i class="fas fa-angle-down"></i>
                                </a>
                                <div class="black-submenu-lv2 collapse multi-collapse" id="multiCollapseExample4">
                                    <div class="card card-body body product__body">
                                        <ul class="third-level">
                                            <li><a href="#"> sticky tabs</a></li>
                                            <li><a href="#"> simple products</a></li>
                                            <li><a href="#"> with left Sidebar</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>


                            <li>


                                <a class="link-sidebar-black2 link product__link collapsed" data-toggle="collapse"
                                    href="#multiCollapseExample5" role="button" aria-expanded="false"
                                    aria-controls="multiCollapseExample5">
                                    <span>product layout TYPES</span><i class="fas fa-angle-down"></i>
                                </a>
                                <div class="black-submenu-lv2 collapse multi-collapse" id="multiCollapseExample5">
                                    <div class="card card-body body product__body">
                                        <ul class="third-level">
                                            <li><a href="#"> default layout</a></li>
                                            <li><a href="#"> extended layout</a></li>
                                            <li><a href="#"> full with layout</a></li>
                                            <li><a href="#"> grid image layout</a></li>
                                            <li><a href="#"> sticky both side info</a><span>hot!</span></li>
                                            <li><a href="#"> sticky right side info</a><span>hot!</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>


                        </ul>


                    </div>
                </div>
            </li>


            <li class="elements elements__pages">
                <a class="link-sidebar-black link pages__link collapsed" data-toggle="collapse"
                    href="#multiCollapseExample6" role="button" aria-expanded="false"
                    aria-controls="multiCollapseExample6">
                    <span>pages</span><span>hot!</span> <i class="fas fa-angle-down"></i>
                </a>
                <div class="black-submenu-lv1 collapse multi-collapse" id="multiCollapseExample6">
                    <div class="card card-body body pages__body">
                        <ul class="sec-level">
                            <li><a href="#">shopping cart </a><span></li>
                            <li>
                                <a class=" link pages__link collapsed" data-toggle="collapse"
                                    href="#multiCollapseExample7" role="button" aria-expanded="false"
                                    aria-controls="multiCollapseExample7">
                                    <span> check out</span><i class="fas fa-angle-down"></i>
                                </a>
                                <div class="row">
                                    <div class="col">
                                        <div class="collapse multi-collapse" id="multiCollapseExample7">
                                            <div class="card card-body body pages__body">
                                                <ul class="third-level">
                                                    <li><a href="#">checkout shipping</a></li>
                                                    <li><a href="#"> checkout shipping 2 </a></li>
                                                    <li><a href="#"> checkout Review</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li><a href="#">about</a></li>

                            <li data-toggle="modal" data-target="#login-modal"><a href="#">log in</a></li>

                            <li><a href="#">forget password</a></li>
                        </ul>
                    </div>
                </div>
            </li>


            <li class="elements elements__blog">
                <a class="link-sidebar-black link blog__link collapsed" data-toggle="collapse"
                    href="#multiCollapseExample8" role="button" aria-expanded="false"
                    aria-controls="multiCollapseExample8">
                    <span>blog</span> <i class="fas fa-angle-down"></i>
                </a>
                <div class="black-submenu-lv1 collapse multi-collapse" id="multiCollapseExample8">
                    <div class="card card-body body blog__body">
                        <ul class="sec-level">
                            <li><a href="#">blog post </a><span></li>
                        </ul>
                    </div>
                </div>
            </li>



            <li class="elements__contactus">
                <a href="#" class="link">contact us</a>
            </li>
            <li class="elements__special">
                <a href="#" class="link">special offer ! <span>hot!</span></a>
            </li>

            <li class="elements">
                <a class="link-sidebar-black link collapsed" data-toggle="collapse" href="#link-lang" role="button"
                    aria-expanded="false" aria-controls="link-lang">
                    <span>lang</span> <i class="fas fa-angle-down"></i>
                </a>
                <div class="black-submenu-lv1 collapse multi-collapse" id="link-lang">
                    <ul>
                        <li>@if (lang() == 'en')
                            <a class="d-block" href="{{ route('lang','ar') }}">ar</a>
                        </li>
                        <li>
                            @else
                            <a class="d-block" href="{{ route('lang','en') }}">en</a>
                            @endif
                        </li>
                    </ul>

                </div>
            </li>

            <li class="elements__buy">
                <a href="#" class="link">buy porto !</a>
            </li>
        </ul>
    </div>


    <div class="layout"></div>


    <!-- navbar  -->
    <nav class="nav-bar" @if(Route::currentRouteName() !=='home' ) @endif>
        <div class="container-fluid navbar-container">
            <div class="nav-bar__left">
                <ul class="nav-bar__left__menu">
                    <?php
                        $jsonString = Storage::get('public\header.json');
                        // $datas = collect($jsonString, true);
                        $datas = json_decode($jsonString, true);
                        // dd($datas);
                    ?>
                    @foreach ($datas as $data)          
                    <li class="nav-bar__left__menu__list">
                        <a href='{{ $data['href']}}' class="{{ request()->is($data['href']) ? 'active' : 'active' }} nav-bar__left__menu__list__links">{{$data['text']}}</a>
                        @if(array_key_exists("children",$data))
                            @foreach ($data['children'] as $item)
                                <div class="nav-bar__left__menu__list__hidden">
                                    <div>
                                        <ul class="first-level">
                                            <li>
                                                <a href="#">{{$item['text']}}</a>
                                            </li>
                                            <li class="sec-level">
                                                <a href='{{ $item['href']}}'>check out</a>
                                                @if(array_key_exists("children",$item))
                                                    @foreach ($item['children'] as $ite)
                                                    <ul class="">
                                                        <li>
                                                            <a href="#">{{$ite['text']}}</a>
                                                        </li>
                                                    </ul>
                                                    @endforeach
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </li>
                    @endforeach
                    <li class="nav-bar__left__menu__hidden-icon" id="menu">
                        <i class="fas fa-bars"></i>
                    </li>
                </ul>
            </div>
            <div class="navbar__logo">
                <?php $logo = Template::select('value')->where('key','header_logo')->get();?>
                @foreach ($logo as $item)
                <a href="/"> <img src="{{ asset('images/backend_images'.'/'.$item->value) }}" class="img">
                </a> 
                @endforeach
            </div>
            <div class="navbar__right">
                <ul class="nav-bar__right__menu">

                    <li class="right__search">
                        <i class="fas fa-search "></i>
                        <div class="child sec-level-search">
                            <form action="{{ url('/search-products') }}" method="post">{{ csrf_field() }}
                                <input type="text" name="product" placeholder="search.....">
                                <div class="form__category">
                                    <select>
                                        <option value="All Categories">All Categories</option>
                                        <option value="Fashion">Fashion</option>
                                        <option value="Women">Women</option>
                                    </select>
                                </div>
                                <button class="btn" type="submit">
                                    <i class="fas fa-search "></i>
                                </button>
                            </form>
                        </div>
                    </li>
                    <li class="nav-bar__right__menu__links">
                        <ul class="nav-bar__right__menu__links__main">
                            <li class="nav-bar__right__menu__links__main__lists">
                                <a class="nav-bar__right__menu__links__main__lists__link" href="#">usd</a>
                                <div>
                                    <ul>
                                        <li>
                                            <a href="#">euro</a>
                                        </li>
                                        <li>
                                            <a href="#">usd</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-bar__right__menu__links__main__lists">
                                <a class="nav-bar__right__menu__links__main__lists__link" href="#">en</a>
                                <div>
                                    <ul>
                                        <li>@if (lang() == 'en')
                                            <a href="{{ route('lang','ar') }}">ar</a>
                                        </li>
                                        <li>
                                            @else
                                            <a href="{{ route('lang','en') }}">en</a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>

                            </li>
                            <li class="nav-bar__right__menu__links__main__lists">
                                <a class="nav-bar__right__menu__links__main__lists__link" href="#">links</a>
                                <div>
                                    <ul>
                                        @if(empty(Auth::check()))
                                        <li><a href="{{ url('/login-register') }}"> Login</a></li>
                                        @else
                                        <li><a href="{{ url('/account') }}"> Account</a></li>
                                        <li><a href="{{ url('/user-logout') }}">Logout</a></li>
                                        <li><a href="{{ url('/wish-list') }}">MY WISHLIST </a></li>
                                         @endif
                                        <!--<li><a href="#">DAILY DEAL</a></li>-->
                                        <!--<li><a href="#">BLOG</a></li>-->
                                        <!--<li><a href="#">Contact</a></li>-->
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-bar__right__menu__phone">
                        <?php $footerContent = Template::where(['key'=>'footer_cont_phone'])->get()?>
                                @foreach ($footerContent as $item)
                                    <a href="tel:{{$item->value}}">{{$item->value}}</a>
                                @endforeach
                    </li>
                    <li class="nav-bar__right__menu__bag-product">
                        <button class="nav-bar__right__menu__bag-product__btn">
                            <span class="d-none">{{ $cartCount }}</span>
                        </button>
                        <div class="nav-bar__right__menu__bag-product__content">
                            {{-- <form name="addtoCartForm" id="addtoCartForm" action="{{ url('add-cart') }}" method="post">
                                {{ csrf_field() }}
                                <div class="hidden-menu-bag">
                                    <div class="nav-bar__right__menu__bag-product__content__head">
                                        <span><span class="nav-bar__right__menu__bag-product__content__head__count"> ({{ $cartCount }})</span>
                                            item (s)</span>
                                        <a href="#">view cart</a>
                                    </div>
                                    <div class="nav-bar__right__menu__bag-product__content__product__empty text-center">
                                        you have no item !
                                    </div>
                                    <div class="nav-bar__right__menu__bag-product__content__product">
                                    </div>
                                    <div class="nav-bar__right__menu__bag-product__content__footer">
                                        <div class="nav-bar__right__menu__bag-product__content__footer__info">
                                            <span>total</span>
                                            <span class="total-price">0</span>
                                        </div>
                                        <div class="nav-bar__right__menu__bag-product__content__footer__btns">
                                            <button type="submit" id="cartButton"
                                            name="cartButton" value="Shopping Cart">
                                            <span> Add to Cart</span>
                                            </button>
                                            <a href="{{ url('/checkout') }}">checkout</a>
                                        </div>
                                    </div>
                                </div>
                            </form> --}}
                        </div>



                    </li>
                </ul>
            </div>
        </div>
    </nav>