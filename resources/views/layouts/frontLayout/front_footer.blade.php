{{-- <footer id="footer"><!--Footer-->

		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Service</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Online Help</a></li>
								<li><a href="{{ url('page/contact') }}">Contact Us</a></li>
<li><a href="#">Order Status</a></li>
<li><a href="#">Change Location</a></li>
<li><a href="#">FAQ’s</a></li>
</ul>
</div>
</div>
<div class="col-sm-2">
    <div class="single-widget">
        <h2>Quock Shop</h2>
        <ul class="nav nav-pills nav-stacked">
            <li><a href="#">T-Shirt</a></li>
            <li><a href="#">Mens</a></li>
            <li><a href="#">Womens</a></li>
            <li><a href="#">Gift Cards</a></li>
            <li><a href="#">Shoes</a></li>
        </ul>
    </div>
</div>
<div class="col-sm-2">
    <div class="single-widget">
        <h2>Policies</h2>
        <ul class="nav nav-pills nav-stacked">
            <li><a href="{{ url('page/terms-conditions') }}">Terms & Conditions</a></li>
            <li><a href="{{ url('page/privacy-policy') }}">Privacy Policy</a></li>
            <li><a href="{{ url('page/refund-policy') }}">Refund Policy</a></li>
            <li><a href="#">Billing System</a></li>
            <li><a href="#">Ticket System</a></li>
        </ul>
    </div>
</div>
<div class="col-sm-2">
    <div class="single-widget">
        <h2>About Shopper</h2>
        <ul class="nav nav-pills nav-stacked">
            <li><a href="{{ url('page/about-us') }}">About Us</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Store Location</a></li>
            <li><a href="#">Affillate Program</a></li>
            <li><a href="#">Copyright</a></li>
        </ul>
    </div>
</div>
<div class="col-sm-3 col-sm-offset-1">
    <div class="single-widget">
        <h2>About Shopper</h2>
        <form action="javascript:void(0);" class="searchform" type="post">{{ csrf_field() }}
            <input onfocus="enableSubscriber();" onfocusout="checkSubscriber();" name="subscriber_email"
                id="subscriber_email" type="email" placeholder="Your email address" required="" />
            <button onclick="checkSubscriber(); addSubscriber();" id="btnSubmit" type="submit"
                class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
            <div id="statusSubscribe" style="display: none;"></div>
            <p>Get the most recent updates from <br />our site and be updated your self...</p>
        </form>
    </div>
</div>

</div>
</div>
</div>

<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All rights reserved.</p>
            <p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span>
            </p>
        </div>
    </div>
</div>

</footer>
<!--/Footer-->

<script>
function checkSubscriber() {
    var subscriber_email = $("#subscriber_email").val();
    $.ajax({
        type: 'post',
        url: '/check-subscriber-email',
        data: {
            subscriber_email: subscriber_email
        },
        success: function(resp) {
            if (resp == "exists") {
                /*alert("Subscriber email already exists");*/
                $("#statusSubscribe").show();
                $("#btnSubmit").hide();
                $("#statusSubscribe").html(
                    "<div style='margin-top:-10px;'>&nbsp;</div><font color='red'>Error: Subscriber email already exists!</font>"
                );
            }

        },
        error: function() {
            alert("Error");
        }
    });
}

function addSubscriber() {
    var subscriber_email = $("#subscriber_email").val();
    $.ajax({
        type: 'post',
        url: '/add-subscriber-email',
        data: {
            subscriber_email: subscriber_email
        },
        success: function(resp) {
            if (resp == "exists") {
                /*alert("Subscriber email already exists");*/
                $("#statusSubscribe").show();
                $("#btnSubmit").hide();
                $("#statusSubscribe").html(
                    "<div style='margin-top:-10px;'>&nbsp;</div><font color='red'>Error: Subscriber email already exists!</font>"
                );
            } else if (resp == "saved") {
                $("#statusSubscribe").show();
                $("#statusSubscribe").html(
                    "<div style='margin-top:-10px;'>&nbsp;</div><font color='green'>Success: Thanks for Subscribing!</font>"
                );
            }

        },
        error: function() {
            alert("Error");
        }
    });
}

function enableSubscriber() {
    $("#btnSubmit").show();
    $("#statusSubscribe").hide();
}
</script> --}}
<?php use App\Template;?>
<footer class="main-footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="footer-top__info">
                        <i class="icon-shipping"></i>
                        <div>
                            <h4>FREE SHIPPING & RETURN</h4>
                            <p>Free shipping on all orders over $99.</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="footer-top__info">
                        <i class="icon-us-dollar"></i>
                        <div>
                            <h4>MONEY BACK GUARANTEE</h4>
                            <p>100% money back guarantee</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="footer-top__info history">
                        <i class="icon-support"></i>
                        <div>
                            <h4>ONLINE SUPPORT 24/7</h4>
                            <p>Lorem ipsum dolor sit amet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="subscribe container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-6 artical">
                        <h4>
                            SUBSCRIBE NEWSLETTER
                        </h4>
                        <p>Get all the latest information on Events,Sales and Offers. Sign up for newsletter today</p>
                    </div>
                    <div class="col-lg-6 p-0 p-md-3 form">
                        <form class="general-form">
                            <input type="email" name="email" class="form-control" placeholder="Email address">
                            <input type="submit" class="btn" value="subscribe">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 fonts">
                <a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a>
                <a href="#" target="_blank"><i class="fab fa-twitter"></i></a>
                <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
            </div>
        </div>
    </div>
    <div class="contact">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3">
                    <div class="contact-info">
                        <h4>CONTACT INFO</h4>
                        <ul>
                            <li class="info">
                                <h6>address:</h6>
                                <?php $footerContent = Template::where(['key'=>'footer_cont_address'])->get()?>
                                @foreach ($footerContent as $item)
                                <p>{{$item->value}}</p>
                                @endforeach
                            </li>
                            <li class="info">
                                <h6>phone:</h6>
                                <?php $footerContent = Template::where(['key'=>'footer_cont_phone'])->get()?>
                                @foreach ($footerContent as $item)
                                    <p>Tell Free: <a href="tel:{{$item->value}}">{{$item->value}}</a></p>
                                @endforeach
                            </li>
                            <li class="info">
                                <h6>EMAIL:</h6>
                                <?php $footerContent = Template::where(['key'=>'footer_cont_email'])->get()?>
                                @foreach ($footerContent as $item)
                                <p><a href="mailto:mail@example.com">{{$item->value}}</a></p>
                                @endforeach
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="col-lg-9">
                    <div class="row sec-part">
                        <div class="col-lg-4">
                            <div class="main-acc">
                                <h4>MY ACCOUNT</h4>
                                <div class="row">
                                    <div class="col-sm-6 links">
                                        <ul>
                                            <li class="link">
                                                <a href="#">About Us</a>
                                            </li>
                                            <li class="link">
                                                <a href="#">Contact Us</a>
                                            </li>
                                            <li class="link">
                                                <a href="#">My Account</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6 links">
                                        <ul>
                                            <li class="link">
                                                <a href="#">Orders History</a>
                                            </li>
                                            <li class="link">
                                                <a href="#">Advanced Search</a>
                                            </li>
                                            <li class="link">
                                                <a href="#">Login</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 col-lg-5">
                            <div class="main-feature">
                                <h4>MAIN FEATURES</h4>
                                <div class="row">
                                    <div class="col-sm-6 links">
                                        <ul>
                                            <li class="link">
                                                <a href="#">Super Fast Magento Theme</a>
                                            </li>
                                            <li class="link">
                                                <a href="#">1st Fully working Ajax Theme</a>
                                            </li>
                                            <li class="link">
                                                <a href="#">20 Unique Homepage Layouts</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6 links">
                                        <ul>
                                            <li class="link">
                                                <a href="#">Powerful Admin Panel</a>
                                            </li>
                                            <li class="link">
                                                <a href="#">Mobile & Retina Optimized</a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="working">
                                <h4>WORKING DAYS/HOURS</h4>
                                <p>Mon - Sun / 9:00AM - 8:00PM</p>
                            </div>
                        </div>
                    </div>
                    <div class="footer-bottom">
                        <div class="all">
                            <div class="link">
                                Achilles eCommerce. © 2018. All Rights Reserved
                            </div>
                            <div class="img">
                                <img src="{{asset('images/payments.png')}}">
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>