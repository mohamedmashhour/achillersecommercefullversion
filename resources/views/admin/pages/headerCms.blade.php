@extends('layouts.adminLayout.admin_design')
@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Products</a> <a href="#" class="current">Add Product</a> </div>
    <h1>Header Menu</h1>
    @if(Session::has('flash_message_error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!! session('flash_message_error') !!}</strong>
            </div>
        @endif   
        @if(Session::has('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!! session('flash_message_success') !!}</strong>
            </div>
        @endif
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>update Header Menu</h5>
          </div>
          <div class="widget-content nopadding">

            <div class="col-md-6">
                {{-- <form action="{{ url('/admin/header-menu')}}" method="POST" >
                    @csrf --}}
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12"><h2>Demo</h2></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading clearfix"><h5 class="pull-left">Menu</h5>
                                        <div class="pull-right">
                                            <button id="btnReload" type="button" class="btn btn-default">
                                                <i class="glyphicon glyphicon-triangle-right"></i> Load Data</button>
                                        </div>
                                    </div>
                                    <div class="panel-body" id="cont">
                                        <ul id="myEditor" class="sortableLists list-group">
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button id="btnOut" style="display:none" type="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Output</button>
                                </div>
                                <div class="form-group">
                                    <form action="{{ url('/admin/header-menu')}}" method="POST">
                                     @csrf
                                        <textarea style="display:none" id="out" class="form-control" cols="50" rows="10" name="value"></textarea>
                                        <button  type="submit" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Updata</button>
                                    </form> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Edit item</div>
                                    <div class="panel-body">
                                        <form id="frmEdit" class="form-horizontal">
                                            <div class="form-group">
                                                <label for="text" class="col-sm-2 control-label">Text</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control item-menu" name="text" id="text" placeholder="Text">
                                                        <div class="input-group-btn">
                                                            <button type="button" id="myEditor_icon" class="btn btn-default" data-iconset="fontawesome"></button>
                                                        </div>
                                                        <input type="hidden" name="icon" class="item-menu">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="href" class="col-sm-2 control-label">URL</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control item-menu" id="href" name="href" placeholder="URL">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="target" class="col-sm-2 control-label">Target</label>
                                                <div class="col-sm-10">
                                                    <select name="target" id="target" class="form-control item-menu">
                                                        <option value="_self">Self</option>
                                                        <option value="_blank">Blank</option>
                                                        <option value="_top">Top</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="title" class="col-sm-2 control-label">Tooltip</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="title" class="form-control item-menu" id="title" placeholder="Tooltip">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="panel-footer">
                                        <button type="button" id="btnUpdate" class="btn btn-primary" disabled><i class="fa fa-refresh"></i> Update</button>
                                        <button type="button" id="btnAdd" class="btn btn-success"><i class="fa fa-plus"></i> Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('customScript')
        
        <script src='{{asset('header/bs-iconpicker/js/iconset/iconset-fontawesome-4.7.0.min.js')}}'></script>
        <script src='{{asset('header/bs-iconpicker/js/bootstrap-iconpicker.js')}}'></script>
        <script src='{{asset('header/jquery-menu-editor.min.js')}}'></script>
        <!--{{-- <script src="{{asset('storage/header.json')}}"> </script> --}}-->
        <script>
            $(document).ready(function () {
                var strjson = [];
                // menu items

                $.ajax({
                    url: "../storage/header.json",
                    dataType: 'json',
                    success: function(data) {

                        var newArray = data.filter(function(index){
                            strjson.push(index);
                        });

                        $('#btnReload').click();

                    }

                });

                


                // var strjson = {{$jsonString}};
                //icon picker options
                var iconPickerOptions = {searchText: 'Buscar...', labelHeader: '{0} de {1} Pags.'};
                //sortable list options
                var sortableListOptions = {
                    placeholderCss: {'background-color': 'cyan'}
                };

                var editor = new MenuEditor('myEditor', {listOptions: sortableListOptions, iconPicker: iconPickerOptions, labelEdit: 'Edit'});
                editor.setForm($('#frmEdit'));
                editor.setUpdateButton($('#btnUpdate'));
                
                $('#btnReload').on('click', function () {
                    editor.setData(strjson);
                });

                $('#btnOut').on('click', function () {
                    var str = editor.getString();
                    $("#out").text(str);
                });

                $("#btnUpdate").click(function(){
                    editor.update();
                });

                $('#btnAdd').click(function(){
                    editor.add();
                    $('#btnOut').click();

                    //return false
                });
                
                    
            });
        </script>
@endsection
