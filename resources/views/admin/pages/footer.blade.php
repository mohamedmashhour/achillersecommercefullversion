@extends('layouts.adminLayout.admin_design')
@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">CMS Pages</a> <a href="#" class="current">Edit CMS Page</a> </div>
    <h1>CMS Footer</h1>
    @if(Session::has('flash_message_error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!! session('flash_message_error') !!}</strong>
            </div>
        @endif   
        @if(Session::has('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!! session('flash_message_success') !!}</strong>
            </div>
        @endif
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Edit Footer Content</h5>
          </div>
          <div class="widget-content nopadding">
          <form method="Post" action="{{url('admin/footer')}}" enctype="multipart/form-data" >
            {{ csrf_field() }}
            
            <input type="phone" name="phone" class="form-control" placeholder="Phone" value="{{$footerphone[0]['value']}}">
            <br/>
            <input type="address" name="address" class="form-control" placeholder="Address" value="{{$footeradd[0]['value']}}">
            <br/>
            <input type="email" name="email" class="form-control" placeholder="Email" value="{{$footermail[0]['value']}}">
            <br/>
            <input type="links" name="links" class="form-control" placeholder="links" value="{{$footerlinks[0]['value']}}">
            <br/>
            <input type="social" name="social" class="form-control" placeholder=" Social" value="{{$footersocial[0]['value']}}">
            <br/>
            <input type="wedget1" name="widget1" class="form-control" placeholder="widget 1" value="{{$footerwid1[0]['value']}}">
            <br/>
            <input type="wedget2" name="widget2" class="form-control" placeholder="widget 2" value="{{$footerwid2[0]['value']}}">
              <button type="submit" class="btn btn-primary">Submit</button>
           </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection