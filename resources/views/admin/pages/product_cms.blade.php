@extends('layouts.adminLayout.admin_design')
@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Products</a> <a href="#" class="current">Add Product</a> </div>
            <h1>Product Images</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif   
            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>update Product Image pages</h5>
                    </div>
                        <div class="widget-content nopadding">
                            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/product-pro')}}" name="pro_image" id="add_product" novalidate="novalidate">{{ csrf_field() }}
                                <div class="control-group">
                                  <label class="control-label">Promotion Image 1 </label>
                                  <input type="hidden" name="key" value="product_promotion1">
                                  <div class="controls">
                                    <div class="uploader" id="uniform-undefined"><input name="pro1" id="image" type="file"></div>
                                  </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Promotion Image 2 </label>
                                    <input type="hidden" name="key" value="product_promotion2">
                                    <div class="controls">
                                      <div class="uploader" id="uniform-undefined"><input name="pro2" id="image" type="file"></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Product Size Guid image</label>
                                    <input type="hidden" name="key" value="product_size_guide">
                                    <div class="controls">
                                      <div class="uploader" id="uniform-undefined"><input name="size" id="image" type="file"></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Shop Banner</label>
                                    <input type="hidden" name="key" value="shop_banner">
                                    <div class="controls">
                                      <div class="uploader" id="uniform-undefined"><input name="shop" id="image" type="file"></div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                  <input type="submit" value="Add Images" class="btn btn-success">
                                </div>
                              </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection