@extends('layouts.frontLayout.front_design')
@section('content')
    <?php use App\Product; ?>
    <div class="container checkout pt-4">
        <ul class="checkout__progress-bar">
            <li class="active">
                <span>Shipping</span>
            </li>
            <li>
                <span data-step="2">Review & Payments</span>
            </li>
        </ul>
        @if(Session::has('flash_message_success'))
	            <div class="alert alert-success alert-block">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_success') !!}</strong>
	            </div>
	        @endif
	        @if(Session::has('flash_message_error'))
	            <div class="alert alert-error alert-block" style="background-color:#f4d2d2">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_error') !!}</strong>
	            </div>
    		@endif  
        <!--data enter-->
        <div class="row products">
            <div class="col-lg-6">
                <ul class="checkout-steps">
                    <li class="checkout-steps__data">
                        <h2>Login</h2>
                        <!-- login form -->
                        <form id="loginForm" name="loginForm" action="{{ url('/user-login') }}" method="POST">{{ csrf_field() }}
                            <div class="form form__email">
                                <label>Email Address </label>
                                <div class="email__input">
                                    <input name="email" type="email" required="">
                                    <span class="input input__data">
                                            <div class="tooltips">
                                                <p>We'll send your order confirmation here.</p>
                                            </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form form__password">
                                <label>Password </label>
                                <div class="input password__input">
                                    <input name="password" type="password" required>
                                </div>
                            </div>
                            <p>You already have an account with us. Sign in or continue as guest.</p>
                            <div class="form__footer">
                                <button type="submit" class="btn btn-primary">log in</button>
                                <a href="{{ url('forgot-password') }}"> Forgot your password?</a>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>

            <div class="col-lg-6">
                <ul class="checkout-steps">
                    <li class="checkout-steps__data">
                        <h2>New User</h2>
                        <!-- RegisterForm -->
                        <form id="registerForm" name="registerForm" action="{{ url('/user-register') }}" method="POST">{{ csrf_field() }}
                            <div class="form name">
                                <label>Name</label>
                                <div class=" input name__input">
                                    <input id="name" name="name" type="text" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="form name">
                                <label>Email</label>
                                <div class="input name__input">
                                    <input id="email" name="email" type="email" placeholder="Email Address" required>
                                </div>
                            </div>
                            <div class="form name">
                                <label>Password</label>
                                <div class="input name__input">
                                    <input id="myPassword" name="password" type="password" placeholder="Password" required>
                                </div>
                            </div>
                            <div class="form__footer">
                                <button type="submit" class="btn btn-primary">Sign Up</button>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>

@endsection
