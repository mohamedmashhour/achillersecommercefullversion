@extends('layouts.frontLayout.front_design')
@section('content')

	    <!-- Start Content reset password Page -->
    
		<div class="container">
			<div class="row">
				@if(Session::has('flash_message_success'))
					<div class="alert alert-success alert-block">
						<button type="button" class="close" data-dismiss="alert">×</button> 
							<strong>{!! session('flash_message_success') !!}</strong>
					</div>
				@endif
				@if(Session::has('flash_message_error'))
					<div class="alert alert-error alert-block" style="background-color:#f4d2d2">
						<button type="button" class="close" data-dismiss="alert">×</button> 
							<strong>{!! session('flash_message_error') !!}</strong>
					</div>
				@endif  
			</div>	
			<div class="reset-form">
			<div class="head">
				<h2>Reset Password</h2>
				<p>Please enter your email address below to receive a password reset link.</p>
			</div>
			<form class="general-form" action="{{ url('/forgot-password') }}" method="POST">{{ csrf_field() }}
				<p>email <span>*</span></p>
				<input name="email" type="email">
				<div>
					<button type="submit">RESET MY PASSWORD</button>
				</div>
			</form>
		  </div>
		</div>
		
		  <!-- End Content reset password Page -->


@endsection