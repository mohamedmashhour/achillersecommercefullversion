@extends('layouts.frontLayout.front_design')
@section('content')


<div class="container dashboard">
	<div class="row">
		<div class="col-md-3 col-12  order-md-first order-last">
			<div class="side-bar">
				<div class="side-bar__widget">
					<h3 class="widget__title">My Account</h3>
					<ul class="widget__list">
						<li><a href="#" class="active account"><i class="fas fa-caret-right"></i> Account Dashboard</a></li>
						<li><a href="#"><i class="fas fa-caret-right"></i> Account Information,</a></li>
						<li><a href="#"><i class="fas fa-caret-right"></i> Address Book </a></li>
						<li><a href="#"><i class="fas fa-caret-right"></i> My Orders </a></li>
						<li><a href="#"><i class="fas fa-caret-right"></i> My Wishlist </a></li>
					</ul>
				</div>
			</div>
		</div>
			<div class="col-md-9 col-12  dashboard-content">
				<h2 class="dashboard-content__tittle">My Dashboard</h2>
				<div class="dashboard-content__alert">
					<p>
						Thank you for registering with Achilles - Premium Template.
					</p>
				</div>
				<div class="dashboard-content__greeting">
					<p>
						Hello, <span>{{ $userDetails->name }}!</span>
						From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information.
						Select a link below to view or edit information.
					</p>
				</div>
				<div class="account">
					<h3 class="account-info__tittle">
						Account Information
					</h3>
					<div class="row">
					<div class="col-lg-6 account-info">
						<div class="account-info__contact">
							<div class="contact__header">
								<h3>
									Contact Information
									</h3>
									<a href="#"><p>edit</p></a>
							</div>
							<div class="contact__body">
								<p>{{ $userDetails->name }}</p>
								<p>{{ $userDetails->email }}</p>
								<p><a href="#">  change password</a></p>
							</div>
						</div>
					</div>
					<!-- shilter account  -->
					<div class="col-lg-6 account-info">
							<div class="account-info__contact">
								<div class="contact__header">
									<h3>
										NEWSLETTERS
										</h3>
										<a href="#"><p>edit</p></a>
								</div>
								<div class="contact__body shilter-body">
									<p>You are currently not subscribed to any newsletter.</p>
								</div>
							</div>
							
						</div>
						</div>
						<!-- adress book  -->
				<div class="account-info">
					<div class="account-info__contact">
						<div class="contact__header">
							<h3>
								ADDRESS BOOK
								</h3>
								<a href="#"><p>edit</p></a>
						</div>
						<div class="contact__body address-bill">
							<p> Default Billing Address</p>
							@if($userDetails->address == '')
							<p>You have not set a default billing address.</p>
							@else
								<p>{{ $userDetails->address }}</p>
								<p>{{ $userDetails->city }}</p>
								<p>{{ $userDetails->state }}</p>
							@endif
							<p><a href="#"> Edit Address</a></p>
						</div>
						<div class="contact__body address-ship">
							<p> Default Shipping Address</p>
							@if($userDetails->address == '')
							<p>You have not set a default shipping address.</p>
							@else
							<p>{{$shippingDetails->address}},{{ $shippingDetails->city }},{{ $shippingDetails->state }}</p>
							@endif
							<p><a href="#"> Edit Address</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection