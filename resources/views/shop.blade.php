<?php
use App\ProductsImage;
use App\Product;
use App\Template;
?>
@extends('layouts.frontLayout.front_design')
@section('content')

<!-- Start breadcrumb Of Fashion Page -->
<div class="breadcrumb-nav">
    <div class="container p-sm-0">
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">

                <li class="breadcrumb-item"><a href="#" title="Back to the frontpage"><img
                            src="{{asset('images/home.svg')}}" alt="home"></a></li>

                <li class="breadcrumb-item"><a href="#" title="Back to the frontpage">fashion</a></li>

                <li class="breadcrumb-item active" aria-current="page">Below$100.100</li>

            </ul>
        </nav>
    </div>
</div>
<!-- End breadcrumb Of Fashion Page -->
<section class="fashion">
    <div class="container p-0">
        <div class="d-flex">
            <!-- Start general-sidebar left content -->
            <div class="general-sidebar__overlay d-none"></div>
            <div class="general-sidebar__responsive-btn">
                <i class="fas fa-sliders-h"></i>
                <i class="fas fa-times d-none"></i>
            </div>
            @include('layouts.frontLayout.front_sidebar')
            <!-- Start fashion right content -->
            <div class="fashion__right">
                <?php $banner = Template::where(['key'=>'shop_banner'])->get()?>
                    @foreach ($banner as $item)
                        <div style="height: 300px;background: url({{asset('images/backend_images/'.$item->value)}}) no-repeat;background-size: 100% 100%;"
                        class="fashion__header"></div>
                    @endforeach
                
                <div class="fashion__content">
                    <div class="container-fluid p-sm-0">
                        <div class="row no-gutters">
                            <div class="col-12">
                                <form action="">
                                    <div class="row no-gutters">
                                        <div
                                            class="col-12 pt-3 pt-md-0 col-sm-6 d-flex justify-content-start align-items-center">
                                            <!--<label class="sort">sort by :</label>
                                            <select class="sort-select">
                                                <option value="1">Featured</option>
                                                <option value="2">best selling</option>
                                                <option value="3">name</option>
                                            </select>-->
                                        </div>
                                        <div
                                            class="col-12 pt-3 pt-md-0 col-sm-6 d-flex justify-content-start justify-content-sm-end align-items-center">
                                            <label class="sort">show:</label>
                                            <select class="sort-select sort-select-numb">
                                                <option value="9">9</option>
                                                <option value="18">18</option>
                                                <option value="27">27</option>
                                                <option value="36">36</option>
                                            </select>
                                            <button type="button" class="opacity-half col-items"><img
                                                    src="{{asset('images/menu.svg')}}" alt="img"></button>
                                            <button type="button" class="row-items"><img
                                                    src="{{asset('images/menu1.svg')}}" alt="img"></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-12">
                                <div class="products-info">
                                    <div class="container-fluid p-0">
                                        {{ csrf_field() }}
                                        <div class="pb-5 append-ajax-content col-products" id="post_data">





                                        </div>
                                        <div class="row">
                                            <div
                                                class="col-12 d-none peload-before-ajax justify-content-center pt-5 pb-4">
                                                <div class='sk-three-bounce'>
                                                    <div class='sk-bounce-1 sk-child'></div>
                                                    <div class='sk-bounce-2 sk-child'></div>
                                                    <div class='sk-bounce-3 sk-child'></div>
                                                </div>
                                            </div>
                                            <div class="col-12 d-none text-center ajax-notfound-items pt-5 pb-4">
                                                <p>No Items Found</p>
                                            </div>

                                            <div class="col-12 d-flex shop-ajax-main-btns justify-content-center pt-5 pb-4">
                                                <div><button class="prev" type="button">previos</button></div>
                                                <div class="shop-ajax-btns d-flex justify-content-center flex-wrap"></div>
                                                <div><button class="next" type="button">Next</button></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('external')
<!-- Start Compare modal of page -->
<div class="modal fade compare-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">comparing box (<span
                        class="compare-modal__count">5</span>)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="pt-1 pb-1 pl-2 alert compare-modal__title alert-success" role="alert">
                    <span class="compare-modal__title__span"></span> has added to wishlist successful
                </div>
                <div class="alert alert-warning d-none compare-modal__warning" role="alert">
                    you have no item in compare
                </div>
                <div class="compare-modal__product">
                    <div class="compare-modal__product__details">
                        <table>

                            <tr class="features">
                                <td>
                                    <span>features</span>
                                </td>
                            </tr>

                            <tr class="avilability">
                                <td>
                                    <span>avilability</span>
                                </td>
                            </tr>

                            <tr class="colors">
                                <td>
                                    <span>colors</span>
                                </td>
                            </tr>

                            <tr class="price">
                                <td>
                                    <span>price</span>
                                </td>
                            </tr>

                            <tr class="options">
                                <td>
                                    <span>sizes</span>
                                </td>
                            </tr>

                            <tr class="action">
                                <td>
                                    <span>action</span>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            <div class="pt-5 pb-4 general-add-new-item d-flex justify-content-center">
                <button>Add New Product</button>
            </div>
        </div>
    </div>
</div>
<!-- End Compare modal of page -->
<!-- Start quck view modal of page -->
<div class="modal fade quick-view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="quick-view__product">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="pb-4 col-12 col-lg-6">
                                <div class="background-right-qickview"></div>

                                <div class="exzoom" id="exzoom">

                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="quick-view__product__right">
                                    <div class="row no-gutters">
                                        <div class="col-12">
                                            <a href="#" class="head">DAILY DEAL, FASHION</a>
                                        </div>
                                        <div class="col-12">
                                            <div class="star">
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                            </div>
                                            <span class="quick-view__product__right__no-review">no reviews</span>
                                        </div>
                                        <div class="col-12">
                                            <div class="par">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione
                                                laborum suscipit, soluta
                                                ullam non sed fugit expedita est ab consectetur enim illo qui in
                                                aperiam animi doloribus
                                                nostrum doloremque fugiat.
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="price">
                                                <span>$80.00</span>
                                            </div>
                                            <div class="details">

                                            </div>
                                        </div>
                                        <div class="col-12 size">
                                            <div class="row no-gutters mt-4 mb-3">
                                                <div class="col-2 d-flex align-items-center">
                                                    <h1>size: </h1>
                                                </div>
                                                <div class="col-10">
                                                    <div class="general-sidebar__left__size">
                                                        <form action="" class="p-0 d-flex">
                                                            <div>
                                                                <label for="med-size" class="label-size">m</label>
                                                                <input id="med-size" type="checkbox">
                                                            </div>
                                                            <div>
                                                                <label for="large-size" class="label-size">l</label>
                                                                <input id="large-size" type="checkbox">
                                                            </div>
                                                            <div>
                                                                <label for="xla-size" class="label-size">xl</label>
                                                                <input id="xla-size" type="checkbox">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 colors">
                                            <div class="row">
                                                <div class="col-2">
                                                    <h1>colors</h1>
                                                </div>
                                                <div class="col-10">
                                                    <div class="mb-3 general-sidebar__left__colors">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 pt-3 pb-3">
                                            <div class="quick-view__product__right__selected-items">
                                                <h1>selected items :</h1>
                                                <span class="quick-view__product__right__selected-items__qty"></span>
                                            </div>
                                        </div>

                                        <div class="col-12 add-product">
                                            <button class="add-product__add-cart" type="button">
                                                <span>add to cart</span>
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('js/frontend_js/vendor/jquery.exzoom.js')}}"></script>
<script>
$(document).ready(function() {

    // compare data image between product and array
    function checkProductImage() {
        $(`img[data-image]`).each(function() {
            var imgAttr = $(this).attr('data-image');
            var relatedProduct = $(this).closest(`.main-coulom`).attr('data-id');

            if (imgAttr != relatedProduct) {
                $(this).remove();
            }

        })
    }

    // function check product size
    function checkProductSize() {
        $(`span[data-size], option[data-size]`).each(function() {
            var spanAttr = $(this).attr('data-size');
            var relatedProduct = $(this).closest(`.main-coulom`).attr('data-id');

            if (spanAttr != relatedProduct) {
                $(this).remove();
            }

        })
    }

    // function chceck If Product Contain Same DataVal OfBag
    function chceckIfProductContainSameDataValOfBag() {
        var allProducts = $(document).find(
            '.nav-bar__right__menu__bag-product__content__product--container[data-val]');
        allProducts.each(function() {
            var mainProduct = $(this).attr('data-val');
            var allProducts = $(document).find('.main-coulom');
            allProducts.each(function() {
                var allAttrs = $(this).attr('id');
                if (allAttrs == mainProduct) {
                    $(this).addClass('product-added');
                }
            });

        });
    }

    // function check same color when call ajax
    function checkSameColor() {
        $('.general-sidebar__left__colors__space input[id]').each(function() {
            $('.general-sidebar__left__colors__space input[id="' + this.id + '"]:gt(0)').closest(
                '.general-sidebar__left__colors__space').remove();
        });
    }

    // function check same size when call ajax
    function checkSameSize() {
        $('.general-sidebar__left__size__space input[id]').each(function() {
            $('.general-sidebar__left__size__space input[id="' + this.id + '"]:gt(0)').closest(
                '.general-sidebar__left__size__space').remove();
        });
    }

    // function return Review to product
    function checkReview() {
        var productStar = $(document).find('.product-content__star');

        productStar.each(function() {
            var spanProductStar = $(this).find('span');
            var productAttr = parseInt($(this).attr('data-star'));

            for (i = 0; i < productAttr; i++) {
                var SelectedSpan = spanProductStar[i];
                SelectedSpan.classList.add('review-star');

            }

        });
    }

    // function cheeck if products less than 1
    function checkProductLength() {
        if ($('.append-ajax-content > *').length == 0) {
            $('.ajax-notfound-items').removeClass('d-none');
        } else {
            $('.ajax-notfound-items').addClass('d-none');
        }
    }




    // general variable for filteration
    var maxLimit = 0;
    var indexBtn = 0;
    var intitalShow = 9;

    function readyDom() {
        $('.peload-before-ajax').removeClass('d-none');
        $('.shop-ajax-main-btns').addClass('opacity-ajax');
        $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
        $.ajax({
            url: "storage/product.json",
            dataType: 'json',
            success: function(data) {
                var allApiData = data.length;
                var numbersOfPages = Math.ceil(allApiData / intitalShow);
                var container = $('<div class="d-flex"/>');
                for(var i = 0; i < numbersOfPages; i++) {
                    container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                }
                $('.shop-ajax-btns').html(container);

                $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                var activePaginationBtn = $('.shop-ajax-btns div button.active');
                
                if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                    $('.shop-ajax-main-btns .next').addClass('disabled');
                    $('.shop-ajax-main-btns .prev').removeClass('disabled');
                } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                    $('.shop-ajax-main-btns .prev').addClass('disabled');
                    $('.shop-ajax-main-btns .next').removeClass('disabled');
                } else {
                    $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    $('.shop-ajax-main-btns .next').removeClass('disabled');
                }

                var BlockThreePoints = `<button class="three-dots">...</button>`;
                var allBtns = $('.shop-ajax-btns div > *');
                var btnsLength = allBtns.length;
                if (btnsLength > 5) {
                    allBtns.hide();
                    allBtns.slice(indexBtn, indexBtn + 5).show();
                    allBtns.slice(-1).show();
                    allBtns.slice(0,1).show();
                }

                var nextItems = activePaginationBtn.nextAll();
                var prevItems = activePaginationBtn.prevAll();


                if (nextItems.length > 5) {
                    allBtns.slice(-1).before(BlockThreePoints);
                } else {
                    allBtns.slice(-1).before('');
                }

                if (prevItems.length > 5) {
                    allBtns.slice(0, 1).after(BlockThreePoints)
                }else {
                    allBtns.slice(0, 1).after('');
                }







                // add color for first onoly first time

                for (i=0; i< data.length;i++) {

                
                    $.each(data[i].product_attr, function(key, value) {
                        var sizeBlock = `<div class="general-sidebar__left__size__space">
                            <label for="${value.size}" class="label-size">${value.size}</label>
                            <input type="radio" name="check-color" id="${value.size}" value="${value.size}">
                        </div>`;

                        $('.general-sidebar__left__size__content').append(sizeBlock);
                    });

                    var colorBlock = `<div class="general-sidebar__left__colors__space">
                        <label style="background-color:#${data[i].product_color}" for="${data[i].product_color}" class="first label-color">
                            <i class="fas fa-check d-none"></i>
                        </label>
                        <input id="${data[i].product_color}" type="checkbox">
                    </div>`;

                    $('.general-sidebar__left__color__content').append(colorBlock);
                }


                //===========================//
                var productAjaxContent = '';
                var emmptyAr = [];
                var resposeProductSize = [];
                var responseProductSize2 = [];

                try {
                    for (var i = maxLimit; i < maxLimit + intitalShow; i++) {

                        $.each(data[i].product_images, function(key, value) {
                            var productHidden =
                                `<img data-image="product${data[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                            emmptyAr.push(productHidden);
                        });

                        $.each(data[i].product_attr, function(key, value) {
                            var spanHidden =
                                `<span data-stock="${value.stock}" data-size="product${data[i].id}" value="${value.size}">${value.size}</span>`;
                            resposeProductSize.push(spanHidden);
                        });

                        $.each(data[i].product_attr, function(key, value) {
                            var spanHidden =
                                `<option data-stock="${value.stock}" data-size="product${data[i].id}" value="${value.size}">${value.size}</option>`;
                            responseProductSize2.push(spanHidden);
                        });

                        


                        productAjaxContent += `<div data-send-id="${data[i].id}" data-code="${data[i].product_code}" data-id="product${data[i].id}" id="product${data[i].id}" data-color="product${data[i].product_color}" data-number="${data[i].price}" class="main-coulom">
                            <div class="product-content">
                                <div class="grid-childs">
                                    <div class="head-content">
                                        <a href="#" class="d-block product-content__img">
                                            <img class="img"
                                                src="images/backend_images/product/medium/${data[i].image}" alt="img">
                                            <img class="img"
                                                src="images/backend_images/product/medium/${data[i].hover_image}"
                                                alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                
                                                <span class="heart" title="add to whishlist"></span>
                                            <span data-class="product${data[i].id}" class="amount" data-toggle="modal"
                                                data-target=".compare-modal">

                                            </span>
                                        </a>
                                    </div>
                                    <div class="coulom-content">
                                        <div class="product-content__info">
                                            <a href="#"
                                                class="d-block product-content__sub-head">${data[i].product_name}</a>
                                            <a href="#"
                                                class="d-block product-content__head">${data[i].product_name}</a>
                                            <div class="product-content__info__hidden-imgs d-none">
                                            </div>
                                            <div class="product-content__info__hidden-sizes d-none">
                                            </div>
                                            <div class="product-content__info__hidden-colors d-none">
                                                <span style="background-color:#${data[i].product_color}" data-color="${data[i].product_color}"></span>
                                            </div>
                                            <div data-star="${data[i].product_rate}" class="product-content__star">
                                                <span><i class="fas fa-fw fa-star"></i></span>
                                                <span><i class="fas fa-fw fa-star"></i></span>
                                                <span><i class="fas fa-fw fa-star"></i></span>
                                                <span><i class="fas fa-fw fa-star"></i></span>
                                                <span><i class="fas fa-fw fa-star"></i></span>
                                            </div>
                                            <div class="product-content__content d-md-none">
                                                ${data[i].description}
                                            </div>
                                            <div class="product-content__price">${data[i].price}
                                            </div>
                                            <div class="product-content__add-cart">
                                                <div class="general-select-number product-content__add-cart__number">
                                                    <span class="minus">
                                                        <i class="fas fa-minus"></i>
                                                    </span>
                                                    <form action="">
                                                        <input type="number" min="1" value="1"
                                                            name="name">
                                                    </form>
                                                    <span class="plus"><i
                                                            class="fas fa-plus"></i></span>
                                                </div>
                                                <a href="/product/${data[i].id}">
                                                    <span>add to cart</span>
                                                </a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

                        $('.append-ajax-content').html(productAjaxContent);

                        var productHiddenImg = $('.main-coulom .product-content__info__hidden-imgs');
                        var emptySized = $('.main-coulom .product-content__info__hidden-sizes');
                        var selectProductSize = $(
                            '.main-coulom .product-content__add-cart__select select');

                        for (n = 0; n < resposeProductSize.length; n++) {
                            emptySized.append(resposeProductSize[n]);
                        }
                        for (r = 0; r < responseProductSize2.length; r++) {
                            selectProductSize.append(responseProductSize2[r]);
                        }
                        for (d = 0; d < emmptyAr.length; d++) {
                            productHiddenImg.append(emmptyAr[d]);
                        }

                        }

                }

                catch(e) {}
                


                


                checkReview();
                chceckIfProductContainSameDataValOfBag();
                checkProductSize();
                checkProductImage();
                checkSameColor();
                checkSameSize();
                checkProductLength();

                $('.peload-before-ajax').addClass('d-none');
                $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                $('html, body').animate({
                    scrollTop: $('.fashion__content').offset().top - 20
                });

                
            }

        });
    }
    readyDom();

    function generalFilter() {
        var allCheckedInputs = $(document).find('.label-color ~ input:checked');
        var maxPriceSelect = $('.max-price-select input').val().trim();
        var AllSizeChecked = $(document).find('.label-size ~ input:checked');
        var allCheckedCat = $('.label-categories ~ input:checked');






        if (allCheckedInputs.length > 0 && maxPriceSelect > 0 && AllSizeChecked.length > 0 && allCheckedCat.length < 1) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];
                    var productArrs = [];
                    allCheckedInputs.each(function(index, checkedInput) {
                        var newArray = data.filter(function(product) {
                            return product.product_color == checkedInput.id && product.price <= maxPriceSelect
                        });
                        productArrs = productArrs.concat(newArray);
                    });


                    var pushedArray = [];
                    var newfilter = productArrs.forEach((obj => {
                        obj.product_attr.forEach(inner_obj => {
                            if (inner_obj.size == AllSizeChecked.attr('id')) {   
                                pushedArray = pushedArray.concat(obj);
                            }
                        });
                    }));


                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    

                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var sizeBlock = `<div class="general-sidebar__left__size__space">
                                    <label for="${value.size}" class="label-size">${value.size}</label>
                                    <input type="radio" name="check-color" id="${value.size}" value="${value.size}">
                                </div>`;

                                $('.general-sidebar__left__size__content').append(
                                    sizeBlock);
                            });

                            var colorBlock = `<div class="general-sidebar__left__colors__space">
                                <label style="background-color:#${pushedArray[i].product_color}" for="${pushedArray[i].product_color}" class="first label-color">
                                    <i class="fas fa-check d-none"></i>
                                </label>
                                <input id="${pushedArray[i].product_color}" type="checkbox">
                            </div>`;

                            $('.general-sidebar__left__color__content').append(colorBlock);


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}
                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
        }
        else if (allCheckedInputs.length > 0 && maxPriceSelect == "" && AllSizeChecked.length < 1 && allCheckedCat.length < 1) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];



                    var pushedArray = [];
                    allCheckedInputs.each(function(index, checkedInput) {
                        var newArray = data.filter(function(product) {
                            return product.product_color == checkedInput.id
                        });
                        pushedArray = pushedArray.concat(newArray);
                    });


                    
                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }

                    
                    
                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}
                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
        }
        else if (allCheckedInputs.length < 1 && maxPriceSelect > 0 && AllSizeChecked.length < 1 && allCheckedCat.length < 1) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];



                    var pushedArray = [];
                    var newArray = data.filter(function(product) {
                        return product.price <= maxPriceSelect
                    });

                    pushedArray = pushedArray.concat(newArray);


                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }


                    
                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });



                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}

                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);

                    
                    


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
        }
        else if (allCheckedInputs.length < 1 && maxPriceSelect == "" && AllSizeChecked.length > 0 && allCheckedCat.length < 1) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({    
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];




                    var pushedArray = [];
                    var newfilter = data.forEach((obj => {
                        obj.product_attr.forEach(inner_obj => {
                            if (inner_obj.size == AllSizeChecked.attr('id')) {   
                                pushedArray = pushedArray.concat(obj);
                            }
                        });
                    }));


                    
                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }

                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });




                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            $('.append-ajax-content').empty();
                            $('.append-ajax-content').append(productAjaxContent);

                            


                            var productHiddenImg = $(
                                '.main-coulom .product-content__info__hidden-imgs');
                            var emptySized = $(
                                '.main-coulom .product-content__info__hidden-sizes');
                            var selectProductSize = $(
                                '.main-coulom .product-content__add-cart__select select');

                            for (n = 0; n < resposeProductSize.length; n++) {
                                emptySized.append(resposeProductSize[n]);
                            }
                            for (r = 0; r < responseProductSize2.length; r++) {
                                selectProductSize.append(responseProductSize2[r]);
                            }
                            for (d = 0; d < emmptyAr.length; d++) {
                                productHiddenImg.append(emmptyAr[d]);
                            }

                            

                        }
                    }
                    catch(e) {}
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });

        }
        else if (allCheckedInputs.length > 0 && maxPriceSelect > 0 && AllSizeChecked.length < 1 && allCheckedCat.length < 1) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({    
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];



                    var pushedArray = [];
                    allCheckedInputs.each(function(index, checkedInput) {
                        var newArray = data.filter(function(product) {
                            return product.product_color == checkedInput.id && product.price <= maxPriceSelect
                        });
                        pushedArray = pushedArray.concat(newArray);
                    });


                    

                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    
                    
                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var sizeBlock = `<div class="general-sidebar__left__size__space">
                                    <label for="${value.size}" class="label-size">${value.size}</label>
                                    <input type="radio" name="check-color" id="${value.size}" value="${value.size}">
                                </div>`;

                                $('.general-sidebar__left__size__content').append(
                                    sizeBlock);
                            });

                            var colorBlock = `<div class="general-sidebar__left__colors__space">
                                <label style="background-color:#${pushedArray[i].product_color}" for="${pushedArray[i].product_color}" class="first label-color">
                                    <i class="fas fa-check d-none"></i>
                                </label>
                                <input id="${pushedArray[i].product_color}" type="checkbox">
                            </div>`;

                            $('.general-sidebar__left__color__content').append(colorBlock);


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }
                    catch(e) {}

                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
            
        }
        else if (allCheckedInputs.length < 1 && maxPriceSelect > 0 && AllSizeChecked.length > 0 && allCheckedCat.length < 1) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({    
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];



                    var productArrs = [];
                    var newArray = data.filter(function(product) {
                        return product.price <= maxPriceSelect
                    });
                    productArrs = productArrs.concat(newArray);


                    var pushedArray = [];
                    var newfilter = productArrs.forEach((obj => {
                        obj.product_attr.forEach(inner_obj => {
                            if (inner_obj.size == AllSizeChecked.attr('id')) {   
                                pushedArray = pushedArray.concat(obj);
                            }
                        });
                    }));


                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    


                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            



                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}
                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkSameColor();
                    checkSameSize();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });

        }
        else if (allCheckedInputs.length > 0 && maxPriceSelect == "" && AllSizeChecked.length > 0 && allCheckedCat.length < 1) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');

            $.ajax({    
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];



                    var productArrs = [];
                    allCheckedInputs.each(function(index, checkedInput) {
                        var newArray = data.filter(function(product) {
                            return product.product_color == checkedInput.id
                        });
                        productArrs = productArrs.concat(newArray);
                    });


                    var pushedArray = [];
                    var newfilter = productArrs.forEach((obj => {
                        obj.product_attr.forEach(inner_obj => {
                            if (inner_obj.size == AllSizeChecked.attr('id')) {   
                                pushedArray = pushedArray.concat(obj);
                            }
                        });
                    }));


                    
                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }



                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}
                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
        }
        else if (allCheckedInputs.length < 1 && maxPriceSelect == "" && AllSizeChecked.length < 1 && allCheckedCat.length < 1) {
            readyDom();
        }


        else if (allCheckedInputs.length > 0 && maxPriceSelect > 0 && AllSizeChecked.length > 0 && allCheckedCat.length > 0) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];
                    var productArrs = [];

                    allCheckedInputs.each(function(index, checkedInput) {
                        var newArray = data.filter(function(product) {
                            return product.product_color == checkedInput.id && product.price <= maxPriceSelect && product.category_id == allCheckedCat.attr('id');
                        });
                        productArrs = productArrs.concat(newArray);
                    });


                    var pushedArray = [];
                    var newfilter = productArrs.forEach((obj => {
                        obj.product_attr.forEach(inner_obj => {
                            if (inner_obj.size == AllSizeChecked.attr('id')) {   
                                pushedArray = pushedArray.concat(obj);
                            }
                        });
                    }));


                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    

                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var sizeBlock = `<div class="general-sidebar__left__size__space">
                                    <label for="${value.size}" class="label-size">${value.size}</label>
                                    <input type="radio" name="check-color" id="${value.size}" value="${value.size}">
                                </div>`;

                                $('.general-sidebar__left__size__content').append(
                                    sizeBlock);
                            });

                            var colorBlock = `<div class="general-sidebar__left__colors__space">
                                <label style="background-color:#${pushedArray[i].product_color}" for="${pushedArray[i].product_color}" class="first label-color">
                                    <i class="fas fa-check d-none"></i>
                                </label>
                                <input id="${pushedArray[i].product_color}" type="checkbox">
                            </div>`;

                            $('.general-sidebar__left__color__content').append(colorBlock);


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}
                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength()
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
        }

        
        else if (allCheckedInputs.length < 1 && maxPriceSelect == "" && AllSizeChecked.length < 1 && allCheckedCat.length > 0) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];


                    
                    
                    var pushedArray = [];
                    var newArray = data.filter(function(product) {
                        
                        return product.category_id == allCheckedCat.attr('id');
                    });

                    
                    pushedArray = pushedArray.concat(newArray);



                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }


                    
                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });



                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}

                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);

                    
                    


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
        }


        else if (allCheckedInputs.length > 0 && maxPriceSelect == "" && AllSizeChecked.length < 1 && allCheckedCat.length > 0) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({    
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];



                    var pushedArray = [];
                    allCheckedInputs.each(function(index, checkedInput) {
                        var newArray = data.filter(function(product) {
                            return product.product_color == checkedInput.id && product.category_id == allCheckedCat.attr('id')
                        });
                        pushedArray = pushedArray.concat(newArray);
                    });


                    

                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    
                    
                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var sizeBlock = `<div class="general-sidebar__left__size__space">
                                    <label for="${value.size}" class="label-size">${value.size}</label>
                                    <input type="radio" name="check-color" id="${value.size}" value="${value.size}">
                                </div>`;

                                $('.general-sidebar__left__size__content').append(
                                    sizeBlock);
                            });

                            var colorBlock = `<div class="general-sidebar__left__colors__space">
                                <label style="background-color:#${pushedArray[i].product_color}" for="${pushedArray[i].product_color}" class="first label-color">
                                    <i class="fas fa-check d-none"></i>
                                </label>
                                <input id="${pushedArray[i].product_color}" type="checkbox">
                            </div>`;

                            $('.general-sidebar__left__color__content').append(colorBlock);


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }
                    catch(e) {}

                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
            
        }

        //

        else if (allCheckedInputs.length < 1 && maxPriceSelect > 0 && AllSizeChecked.length < 1 && allCheckedCat.length > 0) {
            
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({    
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {

                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];



                    var pushedArray = [];
                    var newArray = data.filter(function(product) {
                        return product.category_id == allCheckedCat.attr('id') && product.price <= maxPriceSelect;
                    });
                    pushedArray = pushedArray.concat(newArray);

                    

                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    
                    
                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var sizeBlock = `<div class="general-sidebar__left__size__space">
                                    <label for="${value.size}" class="label-size">${value.size}</label>
                                    <input type="radio" name="check-color" id="${value.size}" value="${value.size}">
                                </div>`;

                                $('.general-sidebar__left__size__content').append(
                                    sizeBlock);
                            });

                            var colorBlock = `<div class="general-sidebar__left__colors__space">
                                <label style="background-color:#${pushedArray[i].product_color}" for="${pushedArray[i].product_color}" class="first label-color">
                                    <i class="fas fa-check d-none"></i>
                                </label>
                                <input id="${pushedArray[i].product_color}" type="checkbox">
                            </div>`;

                            $('.general-sidebar__left__color__content').append(colorBlock);


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }
                    catch(e) {}

                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
            
        }

        else if (allCheckedInputs.length < 1 && maxPriceSelect == "" && AllSizeChecked.length > 0 && allCheckedCat.length > 0) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({    
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];



                    var productArrs = [];
                    var newArray = data.filter(function(product) {
                        return product.category_id == allCheckedCat.attr('id');
                    });
                    productArrs = productArrs.concat(newArray);


                    var pushedArray = [];
                    var newfilter = productArrs.forEach((obj => {
                        obj.product_attr.forEach(inner_obj => {
                            if (inner_obj.size == AllSizeChecked.attr('id')) {   
                                pushedArray = pushedArray.concat(obj);
                            }
                        });
                    }));


                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    


                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            



                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}
                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkSameColor();
                    checkSameSize();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });

        }
        else if (allCheckedInputs.length > 0 && maxPriceSelect > 0 && AllSizeChecked.length < 1 && allCheckedCat.length > 0) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];
                    var pushedArray = [];
                    allCheckedInputs.each(function(index, checkedInput) {
                        var newArray = data.filter(function(product) {
                            return product.product_color == checkedInput.id && product.price <= maxPriceSelect && product.category_id == allCheckedCat.attr('id');
                        });
                        pushedArray = pushedArray.concat(newArray);
                    });


                    


                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    

                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var sizeBlock = `<div class="general-sidebar__left__size__space">
                                    <label for="${value.size}" class="label-size">${value.size}</label>
                                    <input type="radio" name="check-color" id="${value.size}" value="${value.size}">
                                </div>`;

                                $('.general-sidebar__left__size__content').append(
                                    sizeBlock);
                            });

                            var colorBlock = `<div class="general-sidebar__left__colors__space">
                                <label style="background-color:#${pushedArray[i].product_color}" for="${pushedArray[i].product_color}" class="first label-color">
                                    <i class="fas fa-check d-none"></i>
                                </label>
                                <input id="${pushedArray[i].product_color}" type="checkbox">
                            </div>`;

                            $('.general-sidebar__left__color__content').append(colorBlock);


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}
                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength();
                    checkSameColor();
                    checkSameSize();
                    
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
        }
        else if (allCheckedInputs.length > 0 && maxPriceSelect == "" && AllSizeChecked.length > 0 && allCheckedCat.length > 0) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];
                    var productArrs = [];

                    allCheckedInputs.each(function(index, checkedInput) {
                        var newArray = data.filter(function(product) {
                            return product.product_color == checkedInput.id && product.category_id == allCheckedCat.attr('id');
                        });
                        productArrs = productArrs.concat(newArray);
                    });


                    var pushedArray = [];
                    var newfilter = productArrs.forEach((obj => {
                        obj.product_attr.forEach(inner_obj => {
                            if (inner_obj.size == AllSizeChecked.attr('id')) {   
                                pushedArray = pushedArray.concat(obj);
                            }
                        });
                    }));


                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    

                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var sizeBlock = `<div class="general-sidebar__left__size__space">
                                    <label for="${value.size}" class="label-size">${value.size}</label>
                                    <input type="radio" name="check-color" id="${value.size}" value="${value.size}">
                                </div>`;

                                $('.general-sidebar__left__size__content').append(
                                    sizeBlock);
                            });

                            var colorBlock = `<div class="general-sidebar__left__colors__space">
                                <label style="background-color:#${pushedArray[i].product_color}" for="${pushedArray[i].product_color}" class="first label-color">
                                    <i class="fas fa-check d-none"></i>
                                </label>
                                <input id="${pushedArray[i].product_color}" type="checkbox">
                            </div>`;

                            $('.general-sidebar__left__color__content').append(colorBlock);


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}
                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength()
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
        }
        else if (allCheckedInputs.length < 1 && maxPriceSelect > 0 && AllSizeChecked.length > 0 && allCheckedCat.length > 0) {
            $('.peload-before-ajax').removeClass('d-none');
            $('.shop-ajax-main-btns').addClass('opacity-ajax');
            $('.general-sidebar__left .list-unstyled').addClass('opacity-ajax');
            $.ajax({
                url: "storage/product.json",
                dataType: 'json',
                success: function(data) {
                    var productAjaxContent = '';
                    var emmptyAr = [];
                    var resposeProductSize = [];
                    var responseProductSize2 = [];
                    var productArrs = [];

                    allCheckedInputs.each(function(index, checkedInput) {
                        var newArray = data.filter(function(product) {
                            return product.price <= maxPriceSelect && product.category_id == allCheckedCat.attr('id');
                        });
                        productArrs = productArrs.concat(newArray);
                    });


                    var pushedArray = [];
                    var newfilter = productArrs.forEach((obj => {
                        obj.product_attr.forEach(inner_obj => {
                            if (inner_obj.size == AllSizeChecked.attr('id')) {   
                                pushedArray = pushedArray.concat(obj);
                            }
                        });
                    }));


                    // variables for pagination
                    var allApiData = pushedArray.length;
                    var numbersOfPages = Math.ceil(allApiData / intitalShow);
                    var container = $('<div class="d-flex"/>');
                    for(var i = 0; i < numbersOfPages; i++) {
                        container.append('<button type="button" data-pageNum="' + i + '">' + (i + 1) + '</button');
                    }
                    $('.shop-ajax-btns').html(container);

                    $(`.shop-ajax-btns div button:eq(${indexBtn})`).addClass('active disabled');
                    var activePaginationBtn = $('.shop-ajax-btns div button.active');
                    
                    if (activePaginationBtn.is('.shop-ajax-btns div button:last-child')) {
                        $('.shop-ajax-main-btns .next').addClass('disabled');
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                    } else if (activePaginationBtn.is('.shop-ajax-btns div button:first-child')) {
                        $('.shop-ajax-main-btns .prev').addClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    } else {
                        $('.shop-ajax-main-btns .prev').removeClass('disabled');
                        $('.shop-ajax-main-btns .next').removeClass('disabled');
                    }

                    var BlockThreePoints = `<button class="three-dots">...</button>`;
                    var allBtns = $('.shop-ajax-btns div > *');
                    var btnsLength = allBtns.length;
                    if (btnsLength > 5) {
                        allBtns.hide();
                        allBtns.slice(indexBtn, indexBtn + 5).show();
                        allBtns.slice(-1).show();
                        allBtns.slice(0,1).show();
                    }

                    var nextItems = activePaginationBtn.nextAll();
                    var prevItems = activePaginationBtn.prevAll();


                    if (nextItems.length > 5) {
                        allBtns.slice(-1).before(BlockThreePoints);
                    } else {
                        allBtns.slice(-1).before('');
                    }

                    if (prevItems.length > 5) {
                        allBtns.slice(0, 1).after(BlockThreePoints)
                    }else {
                        allBtns.slice(0, 1).after('');
                    }
                    

                    try {
                        for (i = maxLimit; i < maxLimit + intitalShow; i++) {
                        
                        
                            $.each(pushedArray[i].product_images, function(key, value) {
                                var productHidden =
                                    `<img data-image="product${pushedArray[i].id}" src="images/backend_images/product/medium/${value.image}">`;
                                emmptyAr.push(productHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<span data-stock="${value.stock}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</span>`;
                                resposeProductSize.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var spanHidden =
                                    `<option id="${value.size}" data-size="product${pushedArray[i].id}" value="${value.size}">${value.size}</option>`;
                                responseProductSize2.push(spanHidden);
                            });

                            $.each(pushedArray[i].product_attr, function(key, value) {
                                var sizeBlock = `<div class="general-sidebar__left__size__space">
                                    <label for="${value.size}" class="label-size">${value.size}</label>
                                    <input type="radio" name="check-color" id="${value.size}" value="${value.size}">
                                </div>`;

                                $('.general-sidebar__left__size__content').append(
                                    sizeBlock);
                            });

                            var colorBlock = `<div class="general-sidebar__left__colors__space">
                                <label style="background-color:#${pushedArray[i].product_color}" for="${pushedArray[i].product_color}" class="first label-color">
                                    <i class="fas fa-check d-none"></i>
                                </label>
                                <input id="${pushedArray[i].product_color}" type="checkbox">
                            </div>`;

                            $('.general-sidebar__left__color__content').append(colorBlock);


                            var filterProductAttr = pushedArray[i].product_attr.filter(function(index, key, value) {
                                return index.size == AllSizeChecked.attr('id');
                            });
                            

                            productAjaxContent += `<div data-send-id="${pushedArray[i].id}" data-code="${pushedArray[i].product_code}" data-id="product${pushedArray[i].id}" id="product${pushedArray[i].id}" data-color="product${pushedArray[i].product_color}" data-number="${pushedArray[i].price}" class="main-coulom">
                                <div class="product-content">
                                    <div class="grid-childs">
                                        <div class="head-content">
                                            <a href="#" class="d-block product-content__img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].image}" alt="img">
                                                <img class="img"
                                                    src="images/backend_images/product/medium/${pushedArray[i].hover_image}"
                                                    alt="img">
                                                <div class="view text-center" data-toggle="modal"
                                                    data-target=".quick-view">quick view</div>
                                                

                                                    <span class="heart" title="add to whishlist"></span>
                                                <span data-class="product${pushedArray[i].id}" class="amount" data-toggle="modal"
                                                    data-target=".compare-modal">

                                                </span>
                                            </a>
                                        </div>
                                        <div class="coulom-content">
                                            <div class="product-content__info">
                                                <a href="#"
                                                    class="d-block product-content__sub-head">${pushedArray[i].product_name}</a>
                                                <a href="#"
                                                    class="d-block product-content__head">${pushedArray[i].product_name}</a>
                                                <div class="product-content__info__hidden-imgs d-none">
                                                </div>
                                                <div class="product-content__info__hidden-sizes d-none">
                                                </div>
                                                <div class="product-content__info__hidden-colors d-none">
                                                    <span style="background-color:#${pushedArray[i].product_color}" data-color="${pushedArray[i].product_color}"></span>
                                                </div>
                                                <div data-star="${pushedArray[i].product_rate}" class="product-content__star">
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                    <span><i class="fas fa-fw fa-star"></i></span>
                                                </div>
                                                <div class="product-content__content d-md-none">
                                                    ${pushedArray[i].description}
                                                </div>
                                                <div class="product-content__price">${pushedArray[i].price}
                                                </div>
                                                <div class="product-content__add-cart">
                                                    <div class="general-select-number product-content__add-cart__number">
                                                        <span class="minus">
                                                            <i class="fas fa-minus"></i>
                                                        </span>
                                                        <form action="">
                                                            <input type="number" min="1" value="1"
                                                                name="name">
                                                        </form>
                                                        <span class="plus"><i
                                                                class="fas fa-plus"></i></span>
                                                    </div>
                                                    <a href="/product/${pushedArray[i].id}">
                                                        <span>add to cart</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                            

                            

                            

                        }
                    }

                    catch(e) {}
                    

                    $('.append-ajax-content').empty();
                    $('.append-ajax-content').append(productAjaxContent);


                    var productHiddenImg = $(
                        '.main-coulom .product-content__info__hidden-imgs');
                    var emptySized = $(
                        '.main-coulom .product-content__info__hidden-sizes');
                    var selectProductSize = $(
                        '.main-coulom .product-content__add-cart__select select');

                    for (n = 0; n < resposeProductSize.length; n++) {
                        emptySized.append(resposeProductSize[n]);
                    }
                    for (r = 0; r < responseProductSize2.length; r++) {
                        selectProductSize.append(responseProductSize2[r]);
                    }
                    for (d = 0; d < emmptyAr.length; d++) {
                        productHiddenImg.append(emmptyAr[d]);
                    }
                    
                    checkReview();
                    chceckIfProductContainSameDataValOfBag();
                    checkProductSize();
                    checkProductImage();
                    checkProductLength()
                    checkSameColor();
                    checkSameSize();
                    $('.peload-before-ajax').addClass('d-none');
                    $('.shop-ajax-main-btns').removeClass('opacity-ajax');
                    $('.general-sidebar__left .list-unstyled').removeClass('opacity-ajax');
                    
                }

            });
        }
    }


    // function click pagnation buttons
    $(document).on('click', '.shop-ajax-btns button', function() {
        var CurrentPage = parseInt($(this).attr('data-pagenum'));

        indexBtn = (0 + CurrentPage);



        maxLimit = (CurrentPage * 9);

        generalFilter();

        $('html, body').animate({
            scrollTop: $('.append-ajax-content').offset().top - 20
        });
    });


    // function clicl pagination previos
    $(document).on('click', '.shop-ajax-main-btns .prev', function() {
        var ActivePage = $('.shop-ajax-btns button.active');
        ActivePage.prev().click();
    });

    // function clicl pagination next
    $(document).on('click', '.shop-ajax-main-btns .next', function() {
        var ActivePage = $('.shop-ajax-btns button.active');
        ActivePage.next().click();
    });

    // function select numbers of show
    var selectNumber = $('.fashion .fashion__right .fashion__content .sort-select-numb');

    selectNumber.on('change', function () {
        var productOption = parseInt($(this).find('option:selected').val());
        $('.shop-ajax-btns button:first-child').click();
        intitalShow = productOption;
    });


    $('.max-price-select button').click(function() {
        $('.shop-ajax-btns button:first-child').click();

        generalFilter();
    });

      //press enter on text area..
    $('.max-price-select input').keypress(function (e) {
        $('.shop-ajax-btns button:first-child').click();

        var key = e.which;
        if(key == 13)  // the enter key code
        {
            generalFilter();

            return false;  
        }
    });


    // function when select product by color
    $(document).on('click', '.label-color ~ input', function() {
        $('.shop-ajax-btns button:first-child').click();

        generalFilter();
    });

    // function when select product by size
    $(document).on('click', '.label-size ~ input', function() {
        $('.shop-ajax-btns button:first-child').click();

        generalFilter();
    });

    // function when select product by size
    $('.label-categories ~ input').on('click', function() {
        $('.shop-ajax-btns button:first-child').click();

        generalFilter();
    });



});








</script>



@endpush