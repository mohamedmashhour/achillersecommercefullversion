@extends('layouts.frontLayout.front_design')

@section('content')
<?php  
use App\ProductsImage;
use App\ProductsAttribute;
use App\Reviews;    
?>

<div class="owl-carousel owl-theme owl-drag owl-loaded slider">
    @foreach($banners as $key => $banner)
    <div class="first_item @if($key==0) active @endif">
        <a href="{{ $banner->link }}" title="{{ $banner->title}}"><img
                src="images/frontend_images/banners/{{ $banner->image }}"></a>
    </div>
    @endforeach
</div>
<!-- productes  -->

<div class="productes">

    <div class="row row-img">

        <div class="col-12 col-sm-6 col-lg-3 img">
            <a href="#"> <img src="{{ asset('images/banner-1.jpg')}}"></a>
        </div>

        <div class="col-12 col-sm-6 col-lg-3 img">
            <a href="#"> <img src="{{ asset('images/banner-2.jpg')}}"></a>
        </div>

        <div class="col-12 col-sm-6 col-lg-3 img">
            <a href="#"><img src="{{ asset('images/banner-3.jpg')}}"></a>
        </div>

        <div class="col-12 col-sm-6 col-lg-3 img">
            <a href="#"> <img src="{{ asset('images/banner-4.jpg')}}"></a>
        </div>

    </div>
</div>
<!------------------------ men collection  ----------->
<div class="men-collection-container">
    <div class="men-collection  row">

        <!-- owl carousel product  -->
        <div class="col-md-6 men-collection__first-part">
            <div class="pt-5 pb-5 w-75 m-auto position-relative">
                <div class="title pt-2">
                    <h2>New Men's Collection</h2>
                    <p>Yes, this is our new collection, check it out our new arrivals.</p>
                </div>
                <div class="owl-carousel collection owl-theme owl-drag owl-loaded collection slider-container">
                    @foreach($productsAll as $product)
                    <div class="all-product main-coulom">


                        <div class="producte ">

                            <?php $productAltImages = ProductsImage::where('product_id',$product->id)->get();?>
                            @if(count($productAltImages)>0)
                            @foreach($productAltImages as $altimg)
                            <div class="product-content__info__hidden-imgs d-none">
                                <img src="{{ asset('images/backend_images/product/small/'.$altimg->image) }}">
                            </div>
                            @endforeach
                            @endif
                            <?php $productattr = ProductsAttribute::where('product_id',$product->id)->get();?>
                            @if(count($productattr)>0)
                            @foreach($productattr as $attr)

                            <div class="product-content__info__hidden-sizes d-none">
                                <span data-stock="{{$attr->stock}}" value="{{$attr->size}}">{{$attr->size}}</span>
                            </div>
                            @endforeach
                            @endif

                            <div class="product-content__img d-none">
                                <img src="{{ asset('/images/backend_images/product/small/'.$product->image) }}">
                            </div>
                            <a href="{{ url('/product/'.$product->id) }}" class="img">
                                <img src="{{ asset('/images/backend_images/product/small/'.$product->image) }}">
                            </a>
                            <a class="main-layout" href=".quick-view" data-toggle="modal"
                                data-target=".quick-view">quick view</a>
                            <span>-20%</span>
                            <!-- modal bootstarp  -->
                            <!--<span class="logo" data-id="test1" data-src="img/product-1.jpg" data-content="jeans"
                                data-toggle="modal" data-target="#exampleModal">
                            </span>-->
                        </div>
                        <div class="all">
                            <div class="carousel-item-left">
                                <span class="office"><a href="#">{{$product->category_name}}</a></span>
                                <h6><a class="product-content__head" href="#">{{$product->product_name}}</a></h6>
                                <div class="all__rating">
                                    <?php
                                    $product_rate = Reviews::where('product_id',$product->id)->get();

                                    // dd($product_rate ->sum());
                                    // $rate = $product_rate->rate;
                                    $total_product_rate = $product_rate->sum('rate');
                                    $num_product_rate = $product_rate->count('rate');

                                    if($num_product_rate > 0){
                                        $product_rate = $total_product_rate / $num_product_rate;
                                        $product_rate = round($product_rate);
                                    }else{
                                        $product_rate = 0;
                                    }
                                    ?>
                                    <div data-star="{{$product_rate}}" class="product-content__star">
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                        <span><i class="fas fa-fw fa-star"></i></span>
                                    </div>
                                </div>
                                <div class="price">
                                    <span class="old-price">$90 </span>
                                    <span class="new-price product-content__price">{{$product->price}}</span>
                                </div>
                            </div>
                            <div class="heart">
                                <a href="/product/{{$product->id}}"><i class="far fa-heart"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- meaning img  -->
        <div class="col-md-6   col-12 men-collection__sec-part">
            <div class="sec-part__background-men"
                style="min-height:420px;background-image: url({{url('images/bg-1.jpg')}});">
                <div class="title">
                    <h2>Men's <br>Collection</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Features  -->
<div class="project" id="project">
    <div class="define">
        <h3>FEATURED PRODUCTS</h3>
        <p>Check it out our this week's featured products.</p>
    </div>
    <div class="container-fluid">
        <div class="owl-carousel owl-theme owl-drag owl-loaded features">
            @foreach($productsAll as $product)
            <div class="all-product main-coulom">


                <div class="producte ">

                    <?php $productAltImages = ProductsImage::where('product_id',$product->id)->get();?>
                    @if(count($productAltImages)>0)
                    @foreach($productAltImages as $altimg)
                    <div class="product-content__info__hidden-imgs d-none">
                        <img src="{{ asset('images/backend_images/product/small/'.$altimg->image) }}">
                    </div>
                    @endforeach
                    @endif
                    <?php $productattr = ProductsAttribute::where('product_id',$product->id)->get();?>
                    @if(count($productattr)>0)
                    @foreach($productattr as $attr)

                    <div class="product-content__info__hidden-sizes d-none">
                        <span data-stock="{{$attr->stock}}" value="{{$attr->size}}">{{$attr->size}}</span>
                    </div>


                    

                    



                    @endforeach
                    @endif

                    <div class="product-content__info__hidden-colors d-none">
                        <span style="background-color:#{{$product->product_color}}" data-color="{{$product->product_color}}"></span>
                    </div>
                    
                    <div class="product-content__img d-none">
                        <img src="{{ asset('/images/backend_images/product/small/'.$product->image) }}">
                    </div>
                    <a href="{{ url('/product/'.$product->id) }}" class="img">
                        <img src="{{ asset('/images/backend_images/product/small/'.$product->image) }}">
                    </a>
                    <a class="main-layout" href=".quick-view" data-toggle="modal" data-target=".quick-view">quick
                        view</a>
                    <span>-20%</span>
                    <!-- modal bootstarp  -->
                    <!--<span class="logo" data-id="test1" data-src="img/product-1.jpg" data-content="jeans"
                                data-toggle="modal" data-target="#exampleModal">
                            </span>-->
                </div>
                <div class="all">
                    <div class="carousel-item-left">
                        <span class="office"><a href="#">{{$product->category_name}}</a></span>
                        <h6><a class="product-content__head" href="#">{{$product->product_name}}</a></h6>
                        <div class="all__rating">
                            <?php
                                $product_rate = Reviews::where('product_id',$product->id)->get();

                                // dd($product_rate ->sum());
                                // $rate = $product_rate->rate;
                                $total_product_rate = $product_rate->sum('rate');
                                $num_product_rate = $product_rate->count('rate');

                                if($num_product_rate > 0){
                                    $product_rate = $total_product_rate / $num_product_rate;
                                    $product_rate = round($product_rate);
                                }else{
                                    $product_rate = 0;
                                }
                            ?>
                            <div data-star="{{$product_rate}}" class="product-content__star">
                                <span><i class="fas fa-fw fa-star"></i></span>
                                <span><i class="fas fa-fw fa-star"></i></span>
                                <span><i class="fas fa-fw fa-star"></i></span>
                                <span><i class="fas fa-fw fa-star"></i></span>
                                <span><i class="fas fa-fw fa-star"></i></span>
                            </div>
                        </div>
                        <div class="price">
                            <span class="old-price">$90 </span>
                            <span class="new-price product-content__price">{{$product->price}}</span>
                        </div>
                    </div>
                    <div class="heart">
                        <a href="/product/{{$product->id}}"><i class="far fa-heart"></i></a>

                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="modal fade quick-view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="quick-view__product">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="pb-4 col-12 col-lg-6">
                                <div class="background-right-qickview"></div>

                                <div class="exzoom" id="exzoom">

                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="quick-view__product__right">
                                    <div class="row no-gutters">
                                        <div class="col-12">
                                            <a href="#" class="head">DAILY DEAL, FASHION</a>
                                        </div>
                                        <div class="col-12">
                                            <div class="star">
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                            </div>
                                            <span class="quick-view__product__right__no-review">no reviews</span>
                                        </div>
                                        <div class="col-12">
                                            <div class="par">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione
                                                laborum suscipit, soluta
                                                ullam non sed fugit expedita est ab consectetur enim illo qui in
                                                aperiam animi doloribus
                                                nostrum doloremque fugiat.
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="price">
                                                <span>$80.00</span>
                                            </div>
                                            <div class="details">

                                            </div>
                                        </div>
                                        <div class="col-12 size">
                                            <div class="row no-gutters mt-4 mb-3">
                                                <div class="col-2 d-flex align-items-center">
                                                    <h1>size: </h1>
                                                </div>
                                                <div class="col-10">
                                                    <div class="general-sidebar__left__size">
                                                        <form action="" class="p-0 d-flex">
                                                            <div>
                                                                <label for="med-size" class="label-size">m</label>
                                                                <input id="med-size" type="checkbox">
                                                            </div>
                                                            <div>
                                                                <label for="large-size" class="label-size">l</label>
                                                                <input id="large-size" type="checkbox">
                                                            </div>
                                                            <div>
                                                                <label for="xla-size" class="label-size">xl</label>
                                                                <input id="xla-size" type="checkbox">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 colors">
                                            <div class="row">
                                                <div class="col-2">
                                                    <h1>colors</h1>
                                                </div>
                                                <div class="col-10">
                                                    <div class="mb-3 general-sidebar__left__colors">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <!--<div class="col-12 add-product">
                                            <button class="add-product__add-cart" type="button">
                                                <span>add to cart</span>
                                            </button>
                                        </div>-->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ asset('js/frontend_js/vendor/jquery.exzoom.js')}}"></script>

<script src="js/frontend_js/pages/home.js"></script>
@endpush